//
//  UpdateController.swift
//  VuforiaLastTry
//
//  Created by user on 24/01/2019.
//  Copyright © 2019 Beliverz. All rights reserved.
//

import UIKit

class UpdateController: MainController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate {

    @IBOutlet weak var fnameView: UIView!
    @IBOutlet weak var lnameView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var AgeView: UIView!
    @IBOutlet weak var DesciptionView: UIView!
    
    @IBOutlet weak var ProfileImage: UIImageView!
    @IBOutlet weak var CoverImage: UIImageView!
    
    @IBOutlet weak var fnameLbl: UILabel!
    @IBOutlet weak var fnameTF: UITextField!
    
    @IBOutlet weak var lname: UILabel!
    @IBOutlet weak var lnameTF: UITextField!
    
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var phoneTF: UITextField!
    
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var ageTF: UITextField!
    
    @IBOutlet weak var Des: UILabel!
    @IBOutlet weak var DescriptionTF: UITextView!
    
    var profiledata = [ShowProfileDatum]()
    var picker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fnameTF.becomeFirstResponder()
        HideCard(card: fnameView)
        fnameTF.frame.size.height = 24
        fnameLbl.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        fnameTF.delegate = self
        lnameTF.delegate = self
        phoneTF.delegate = self
        ageTF.delegate = self
        DescriptionTF.delegate = self
        
        ProfileImage.layer.cornerRadius = ProfileImage.frame.height/2
        ProfileImage.layer.borderColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
        fnameView.layer.shadowOpacity = 0.2
        fnameView.layer.shadowOffset.height = 1
        fnameView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        fnameTF.frame.size.height = 28
        fnameTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
        fnameLbl.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
        ShowProfileData()
       
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        applyCurvedPath(givenView: CoverImage, curvedPercent: 0.35)
    }
    
    func ShowProfileData(){
        if ChectInternet() == true{
            showActivityIndicatory(uiView: self.view)
            showProfileModel = ShowProfileViewModel(user_id: user_id, profile_id: user_id)
            showProfileModel.showProfile { (bool, data) in
                if bool == true{
                    
                    self.fnameTF.text = data[0].fname ?? ""
                    self.lnameTF.text = data[0].lname ?? ""
                    self.ageTF.text = String(data[0].age ?? 0)
                    self.DescriptionTF.text = data[0].about ?? ""
                    self.phoneTF.text = data[0].phoneNo ?? ""
                    self.about = data[0].about ?? ""
                    if let cover = data[0].coverImage{
                        self.CoverImage.kf.setImage(with: URL(string: "https://shoclefapi.shoclef.com" + cover))
                    }
                    if let profile = data[0].profielImage{
                        self.ProfileImage.kf.setImage(with: URL(string: "https://shoclefapi.shoclef.com" + profile))
                    }
                    self.HideActivityIndicator(uiView: self.view)
                }else{
                    self.HideActivityIndicator(uiView: self.view)
                    self.AlertDismiss(title: "Sorry", message: "Server Didn't find your profile data")
                   }
             }
        }
    }
    
    
    @IBAction func  updateBtn(){
        if ChectInternet() == true{
            if fnameTF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
            {
                customAlert("Error", "Enter First Name...")
            }
            else if lnameTF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
            {
                customAlert("Error", "Enter Last Name...")
            }
            else if ageTF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
            {
                customAlert("Error", "Enter Age...")
            }
            else if phoneTF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
            {
                customAlert("Error", "Enter Phone No...")
            }
            else if DescriptionTF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
            {
                customAlert("Error", "Enter Some Description...")
            }
            else if CoverImage.image == nil{
                
                customAlert("Error", "Choose your cover photo")
                
            }else if ProfileImage.image == nil{
                
                customAlert("Error", "Choose your profile photo")
                
            }
            else{
                showActivityIndicatory(uiView: self.view)
                updateProfileModel = UpdateProfileViewModel(fname: fnameTF.text?.trimmingCharacters(in: .whitespaces) ?? "", lname: lnameTF.text?.trimmingCharacters(in: .whitespaces) ?? "", coverImage: (CoverImage.image?.convertBaser64Withoutsize(image: CoverImage.image!))!, profileImage: (ProfileImage.image?.convertBaser64Withoutsize(image: ProfileImage.image!))!, about: DescriptionTF.text.trimmingCharacters(in: .whitespaces), phone: phoneTF.text?.trimmingCharacters(in: .whitespaces) ?? "", age: Int(ageTF.text?.trimmingCharacters(in: .whitespaces) ?? "") ?? 0, user_id: user_id)
                updateProfileModel.updateProfile { (bool, data) in
                    if bool == true{
                        self.customAlert("Success", data)
                        self.ShowProfileData()
                    }else{
                        self.HideActivityIndicator(uiView: self.view)
                        self.customAlert("Sorry", data)
                    }
                }
            }
        }
    }
    @IBAction func CoverBtn(_ sender: UIBarButtonItem) {
        pic = "cover"
        let alert = UIAlertController(title: "Choose Option", message: "Upload from libray or Capture from camera", preferredStyle: .actionSheet)
        let Add = UIAlertAction(title: "Camera", style: .default, handler: {
            UIAlertAction in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker.delegate = self
                self.picker.allowsEditing = false
                self.picker.sourceType = .camera
                self.present(self.picker, animated: true, completion: nil)
            }
        })
        
        let load = UIAlertAction(title: "Upload Photos", style: .default, handler: {UIAlertAction in
            self.picker.delegate = self
            self.picker.allowsEditing = false
            self.picker.sourceType = .photoLibrary
            self.present(self.picker, animated: true, completion: nil)
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        //  Add.setValue(UIImage(named: "camera@3x.png")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
        //  load.setValue(UIImage(named: "gallery@3x.png")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
        alert.addAction(load)
        alert.addAction(Add)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func ProfileBtn(_ sender: UIBarButtonItem) {
        pic = "profile"
        let alert = UIAlertController(title: "Choose Option", message: "Upload from libray or Capture from camera", preferredStyle: .actionSheet)
        let Add = UIAlertAction(title: "Camera", style: .default, handler: {
            UIAlertAction in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker.delegate = self
                self.picker.allowsEditing = true
                self.picker.sourceType = UIImagePickerController.SourceType.camera
                self.present(self.picker, animated: true, completion: nil)
            }
        })
        
        let load = UIAlertAction(title: "Upload Photos", style: .default, handler: {UIAlertAction in
            self.picker.delegate = self
            self.picker.allowsEditing = true
            self.picker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(self.picker, animated: true, completion: nil)
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        //   Add.setValue(UIImage(named: "camera@3x.png")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
        //  load.setValue(UIImage(named: "gallery@3x.png")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
        alert.addAction(load)
        alert.addAction(Add)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    var pic = "cover"
    var pImage : UIImage? = nil
    var cImage : UIImage? = nil
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        if let pickedImage = info[.originalImage] as? UIImage{
            if pic == "cover"{
                CoverImage.contentMode = .scaleAspectFill
                CoverImage.image = pickedImage
                cImage = pickedImage
            }
            else{
                ProfileImage.contentMode = .scaleAspectFill
                ProfileImage.image = pickedImage
                pImage = pickedImage
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if fnameTF == textField{
            ShowCard(card: fnameView)
            HideCard(card: AgeView)
            HideCard(card: lnameView)
            HideCard(card: phoneView)
            HideCard(card: DesciptionView)
           
            fnameTF.frame.size.height = 28
            fnameTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            fnameLbl.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
            textField.becomeFirstResponder()
        }
        if  textField == lnameTF{
            ShowCard(card: lnameView)
            HideCard(card: AgeView)
            HideCard(card: fnameView)
            HideCard(card: phoneView)
            HideCard(card: DesciptionView)
            
            lnameTF.frame.size.height = 28
            lnameTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            lname.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
            textField.becomeFirstResponder()
        }
        if  textField == ageTF{
            ShowCard(card: AgeView )
            HideCard(card: lnameView)
            HideCard(card: fnameView)
            HideCard(card: phoneView)
            HideCard(card: DesciptionView)
            
            ageTF.frame.size.height = 28
            ageTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            age.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
            textField.becomeFirstResponder()
        }
        if textField == phoneTF{
            ShowCard(card: phoneView)
            HideCard(card: lnameView)
            HideCard(card: fnameView)
            HideCard(card: AgeView)
            HideCard(card: DesciptionView)
            phoneTF.frame.size.height = 28
            phoneTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            phone.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
            textField.becomeFirstResponder()
        }
        
        if textField == DescriptionTF{
            ShowCard(card: DesciptionView)
            HideCard(card: lnameView)
            HideCard(card: fnameView)
            HideCard(card: AgeView)
            HideCard(card: phoneView)
            
            DescriptionTF.frame.size.height = 28
            DescriptionTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            Des.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
            textField.becomeFirstResponder()
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == fnameTF{
            HideCard(card: fnameView)
            fnameTF.frame.size.height = 24
            fnameLbl.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if  textField == lnameTF{
            HideCard(card: lnameView)
            lnameTF.frame.size.height = 24
            lname.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if  textField == ageTF{
            HideCard(card: AgeView)
            ageTF.frame.size.height = 24
            age.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if textField == phoneTF{
            HideCard(card: phoneView)
            phoneTF.frame.size.height = 24
            phone.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if textField == DescriptionTF{
            HideCard(card: DesciptionView)
            DescriptionTF.frame.size.height = 24
            Des.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == fnameTF{
            HideCard(card: fnameView)
            fnameTF.frame.size.height = 24
            fnameLbl.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if  textField == lnameTF{
            HideCard(card: lnameView)
            lnameTF.frame.size.height = 24
            lname.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if  textField == ageTF{
            HideCard(card: AgeView)
            ageTF.frame.size.height = 24
            age.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if textField == phoneTF{
            HideCard(card: phoneView)
            phoneTF.frame.size.height = 24
            phone.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if textField == DescriptionTF{
            HideCard(card: DesciptionView)
            DescriptionTF.frame.size.height = 24
            Des.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        textField.resignFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        DescriptionTF.resignFirstResponder()
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "No Description"
        {
            textView.text = ""
        }
        
        textView.becomeFirstResponder()
    }
    var about = String()
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            if (textView.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
            {
                textView.text = about
                textView.resignFirstResponder()
                return false
            }
            else
            {
                return true
            }
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == fnameTF {
            let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            
            let Range = range.length + range.location > (fnameTF.text?.count)!
            
            if Range == false && alphabet == false {
                return false
            }
            
            
            let NewLength = (fnameTF.text?.count)! + string.count - range.length
            return NewLength <= 20
        } else if textField == lnameTF {
            let allowedCharacters = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharaterSet = CharacterSet(charactersIn: string)
            let alphabet =  allowedCharacterSet.isSuperset(of: typedCharaterSet)
            
            
            let Range = range.length + range.location > (lnameTF.text?.count)!
            
            if Range == false && alphabet == false {
                return false
            }
            
            
            let NewLength = (lnameTF.text?.count)! + string.count - range.length
            return NewLength <= 20
        } else if textField == phoneTF {
            if range.length + range.location > (phoneTF.text?.count)! {
                return false
            }
            let NewLength = (phoneTF.text?.count)! + string.count - range.length
            return NewLength <= 14
        } else if textField == ageTF {
            if range.length + range.location > (ageTF.text?.count)! {
                return false
            }
            let NewLength = (ageTF.text?.count)! + string.count - range.length
            return NewLength <= 2
        } else {
            return true
        }
    }
    
    func ShowCard(card: UIView)
    {
        card.layer.shadowOpacity = 0.2
        card.layer.shadowOffset.height = 1
        card.layer.cornerRadius = 15
        card.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func HideCard(card: UIView){
        card.layer.shadowOpacity = 0
        card.layer.shadowOffset.height = 0
        card.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
    }
}
