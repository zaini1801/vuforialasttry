//
//  ForgetPasswordVC.swift
//  Interactive Wall
//
//  Created by Umer Khalid on 22/10/2018.
//  Copyright © 2018 Nomi Malik. All rights reserved.
//

import UIKit

class ForgetPasswordVC: MainController,UITextFieldDelegate {

    @IBOutlet weak var emailAddressTF: UITextField!
    @IBOutlet weak var emailAdressView: UIView!
    @IBOutlet var mainView: UIView!
     var tapGesture = UITapGestureRecognizer()
    override func viewDidLoad() {
        super.viewDidLoad()

        
    tapGesture = UITapGestureRecognizer(target: self, action: #selector(myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        mainView.addGestureRecognizer(tapGesture)
        mainView.isUserInteractionEnabled = true
        emailAddressTF.delegate = self
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        
        self.resignFirstResponder()
        HideCard(card: emailAdressView)
    }
    
    func ForgetPass(){
        
        emailAddressTF.resignFirstResponder()
        if ChectInternet() == true{
            if (emailAddressTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
                customAlert("Alert", "Please enter your recovery email")
            }else if !isValidEmail(testStr: emailAddressTF.text!)
            {
                customAlert("Error", "Enter Valid Email Address.")
            }
            else{
                showActivityIndicatory(uiView: self.view)
                forgetModel = ForgetPassViewModel(email: emailAddressTF.text?.removeSpace() ?? "")
                forgetModel.forgetPassword(completion: { (bool, data) in
                    if bool == true{
                       self.HideActivityIndicator(uiView: self.view)
                        self.AlertDismiss(title: "Success", message: data)
                    }else{
                        self.HideActivityIndicator(uiView: self.view)
                        self.customAlert("Sorry", data)
                    }
                })
            }
        }
    }
    
    @IBAction func SaveBtn(){
        ForgetPass()
    }

    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if emailAddressTF == textField{
            ShowCard(card: emailAdressView)
            
            emailAddressTF.frame.size.height = 28
            //            purchaseTShirtTextField.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            //            transactionName.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
        }
        
        
        textField.becomeFirstResponder()
    }
    
    
    func hideField(field: UITextField, lbl: UILabel){
        field.frame.size.height = 24
        lbl.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
    }
    func ShowCard(card: UIView)
    {
        card.layer.shadowOpacity = 0.2
        card.layer.shadowOffset.height = 1
        card.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func HideCard(card: UIView){
        card.layer.shadowOpacity = 0
        card.layer.shadowOffset.height = 0
        card.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
