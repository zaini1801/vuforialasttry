//
//  ChooseHomeController.swift
//  Interactive Wall
//
//  Created by user on 17/01/2019.
//  Copyright © 2019 Nomi Malik. All rights reserved.
//

import UIKit

class ChooseHomeController: MainController {

//        @IBOutlet weak var personalShopperLabel:UILabel!
//        @IBOutlet weak var businessLabel:UILabel!
//        @IBOutlet weak var playerLabel:UILabel!
//        @IBOutlet weak var serviceLabel:UILabel!
    
        override func viewDidLoad() {
            super.viewDidLoad()
            
            // Do any additional setup after loading the view.
            
//            personalShopperLabel.layer.cornerRadius = 4.26
//            businessLabel.layer.cornerRadius = 4.26
//            playerLabel.layer.cornerRadius = 4.26
//            serviceLabel.layer.cornerRadius = 4.26
//            personalShopperLabel.clipsToBounds = true
            
        }
        
        override func viewWillLayoutSubviews() {
//            personalShopperLabel.sizeToFit()
//            businessLabel.sizeToFit()
//            playerLabel.sizeToFit()
//            serviceLabel.sizeToFit()
        }
        
        @IBAction func hideShowPersonalShopperLabel(sender:UIButton)
        {
            let story = UIStoryboard(name: "HomePage", bundle: nil)
            if sender.tag == 0
            {
                
//                personalShopperLabel.isHidden = personalShopperLabel.isHidden == false ? true : false
//                businessLabel.isHidden = true
//                serviceLabel.isHidden = true
//                playerLabel.isHidden = true
                
            }
            if sender.tag == 1
            {
                let createWall = story.instantiateViewController(withIdentifier: "UploadTargetVC") as! UploadTargetVC
                present(createWall, animated: true, completion: nil)
//                businessLabel.isHidden = businessLabel.isHidden == false ? true : false
//                personalShopperLabel.isHidden = true
//                serviceLabel.isHidden = true
//                playerLabel.isHidden = true
            }
            if sender.tag == 2
            {
                let story = UIStoryboard(name: "HomePage", bundle: nil)
                let scan = story.instantiateViewController(withIdentifier: "UploadTargetVC") as! UploadTargetVC
                present(scan, animated: true, completion: nil)
//                playerLabel.isHidden = playerLabel.isHidden == false ? true : false
//                businessLabel.isHidden = true
//                serviceLabel.isHidden = true
//                personalShopperLabel.isHidden = true
            }
            if sender.tag == 3
            {
//                serviceLabel.isHidden = serviceLabel.isHidden == false ? true : false
//                businessLabel.isHidden = true
//                personalShopperLabel.isHidden = true
//                playerLabel.isHidden = true
            }
            
        }
    
    @IBAction func Logout(){
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.removeObject(forKey: "userid")
        UserDefaults.standard.removeObject(forKey: "name")
        UserDefaults.standard.removeObject(forKey: "pic")
        let login = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        present(login, animated: true, completion: nil)
        self.view.removeFromSuperview()
    }
    
}

