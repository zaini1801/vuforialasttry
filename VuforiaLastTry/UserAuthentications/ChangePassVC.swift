//
//  ChangePassVC.swift
//  Interactive Wall
//
//  Created by Umer Khalid on 22/10/2018.
//  Copyright © 2018 Nomi Malik. All rights reserved.
//

import UIKit

class ChangePassVC: MainController,UITextFieldDelegate {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var newView: UIView!
    @IBOutlet weak var confirmView: UIView!
    
    @IBOutlet weak var current: UILabel!
    @IBOutlet weak var currentTF: UITextField!
    
    @IBOutlet weak var New: UILabel!
    @IBOutlet weak var NewTF: UITextField!
    
    @IBOutlet weak var confirm: UILabel!
    @IBOutlet weak var confirmTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentTF.delegate = self
        NewTF.delegate = self
        confirmTF.delegate = self
        backView.layer.shadowOpacity = 0.2
        backView.layer.shadowOffset.height = 1
        backView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        currentTF.frame.size.height = 28
        currentTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
        current.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
    }
    
    
    
    func ChangePass(){
        if ChectInternet() == true{
            if (currentTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
                customAlert("Alert", "Please enter current password")
            } else if (NewTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
                customAlert("Alert", "Please enter new password")
            }else if
        (confirmTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
                customAlert("Alert", "Please enter password to confirm")
            } else{
                showActivityIndicatory(uiView: self.view)
                if confirmTF.text?.trimmingCharacters(in: .whitespaces) == NewTF.text?.trimmingCharacters(in: .whitespaces){
                    showActivityIndicatory(uiView: self.view)
                    changePassModel = ChangePassViewModel(user_id: user_id, oldPass: currentTF.text?.trimmingCharacters(in: .whitespaces) ?? "", newPass: NewTF.text?.trimmingCharacters(in: .whitespaces) ?? "")
                    changePassModel.ChangePass { (bool, data) in
                        if bool == true{
                            self.HideActivityIndicator(uiView: self.view)
                            self.AlertDismiss(title: "Success", message: data)
                        }else{
                            self.HideActivityIndicator(uiView: self.view)
                            self.customAlert("Sorry", data)
                        }
                    }
                }else{
                    customAlert("Alert", "Password didn't same")
                }
            }
        }
    }
    
    
    @IBAction func SubmitBtn(){
        ChangePass()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if currentTF == textField{
            ShowCard(card: backView)
            HideCard(card: newView)
            HideCard(card: confirmView)
            currentTF.frame.size.height = 28
            currentTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            current.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
        }
        if  textField == NewTF{
            ShowCard(card: newView)
            HideCard(card: backView)
            HideCard(card: confirmView)
            NewTF.frame.size.height = 28
            NewTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            New.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
            
        }
        if  textField == confirmTF{
            ShowCard(card: confirmView)
            HideCard(card: backView)
            HideCard(card: newView)
            confirmTF.frame.size.height = 28
            confirmTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            confirm.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
        }
        textField.becomeFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == currentTF{
            HideCard(card: backView)
            currentTF.frame.size.height = 24
            current.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
            currentTF.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
            
        }
        if  textField == NewTF{
            HideCard(card: newView)
            NewTF.frame.size.height = 24
            New.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
            NewTF.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
        }
        if  textField == confirmTF{
            HideCard(card: confirmView)
            confirmTF.frame.size.height = 24
            confirm.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
            confirmTF.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == currentTF{
            HideCard(card: backView)
            currentTF.frame.size.height = 24
            current.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
            currentTF.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
        }
        if  textField == NewTF{
            HideCard(card: newView)
            NewTF.frame.size.height = 24
            New.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
            NewTF.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
        }
        if  textField == confirmTF{
            HideCard(card: confirmView)
            confirmTF.frame.size.height = 24
            confirm.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
            confirmTF.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
        }
        textField.resignFirstResponder()
        return true
    }
    
    func ShowCard(card: UIView)
    {
        card.layer.shadowOpacity = 0.2
        card.layer.shadowOffset.height = 1
        card.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func HideCard(card: UIView){
        card.layer.shadowOpacity = 0
        card.layer.shadowOffset.height = 0
        backView.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
