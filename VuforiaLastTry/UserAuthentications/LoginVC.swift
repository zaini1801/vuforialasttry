//
//  LoginVC.swift
//  Interactive Wall
//
//  Created by Umer Khalid on 22/10/2018.
//  Copyright © 2018 Nomi Malik. All rights reserved.
//

import UIKit

class LoginVC: MainController {

    var checked = false
    @IBOutlet weak var googleLoginButton: UIButton!
    @IBOutlet weak var usernameView: UITextField!
    @IBOutlet weak var usernameViewHolder: UIView!
    @IBOutlet weak var passwordView: UITextField!
    @IBOutlet weak var passwordViewHolder: UIView!
    @IBOutlet weak var loginButtonView: UIButton!
    @IBOutlet weak var fBcustomButton: UIButton!
    //@IBOutlet weak var fbloginButton: FBSDKLoginButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var checkboxButton: UIButton!
    @IBOutlet weak var termCond: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        termCond.isUserInteractionEnabled = true
    }
    

    @IBAction func checkboxButtonSelected(_ sender: Any) {
        if !checked {
            checkboxButton.setImage(UIImage(named: "checkboxSelected.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
            checkboxButton.tintColor = UIColor.blue
            checked = true
        } else if checked {
            checkboxButton.setImage(UIImage(named: "CheckBox Log"), for: .normal)
            checkboxButton.tintColor = UIColor.white
            checked = false
        }
    }
    
    @IBAction func termandCond(_ tap: UITapGestureRecognizer)
    {
        print("terms and conditions")
        if let vc = UIStoryboard(name: "HomePage", bundle: nil).instantiateViewController(withIdentifier: "TermsController") as? TermsController{
            self.present(vc, animated: true, completion: nil)
        }
    }
    


    @IBAction func dismissKeyBoard(_ sender: UITapGestureRecognizer) {
        self.scrollView.endEditing(true)
    }
    
    @IBAction func loginButton(_ sender: Any) {
        //  Crashlytics.sharedInstance().crash()
        //check if the text fields are empty
        if  (usernameView.text?.trimmingCharacters(in: .whitespaces) == "") || (passwordView.text?.trimmingCharacters(in: .whitespaces) == "") {
            customAlert("Error", "Enter all the fields")
            
        }else if checked == false
        {
            customAlert("Error", "Please agree to our T&C.")
        }
        else {
            getLogin()
        }
        
    }
    
    func getLogin(){
        if ChectInternet() == true{
            showActivityIndicatory(uiView: self.view)
            loginModel = LoginViewModel(username: usernameView.text ?? "", password: passwordView.text ?? "")
            loginModel.SignIn { (bool, data) in
                if bool == true{
                    self.defaults.set(true, forKey: "isUserLoggedIn")
                    self.defaults.synchronize()
                    self.defaults.set(data[0].userID, forKey: "userid")
                    self.defaults.set("\(String(describing: data[0].firstName)) \(String(describing: data[0].lastName))" , forKey: "name")
                    self.defaults.set(data[0].profilePicture, forKey: "pic")
                    let home = UIStoryboard(name: "HomePage", bundle: nil).instantiateViewController(withIdentifier: "HomePageViewController") as! HomePageViewController
                    self.present(home, animated: true, completion: nil)
                    self.HideActivityIndicator(uiView: self.view)
                }else{
                    self.HideActivityIndicator(uiView: self.view)
                    self.customAlert("Failed", "Try Again")
                }
            }
        }
    }
}


extension LoginVC:UITextFieldDelegate{
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == usernameView {
            self.usernameViewHolder.setViewCard()
        }
        if textField == passwordView {
            self.passwordViewHolder.setViewCard()
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == usernameView {
            self.usernameViewHolder.removeCardView()
        }
        if textField == passwordView {
            self.passwordViewHolder.removeCardView()
        }
    }
}
