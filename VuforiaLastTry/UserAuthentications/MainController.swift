//
//  MainController.swift
//  Interactive Wall
//
//  Created by user on 15/01/2019.
//  Copyright © 2019 Nomi Malik. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD

class MainController: UIViewController {

    let defaults = UserDefaults.standard
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
     var loginModel : LoginViewModel!
     var signUpModel : SignUPViewModel!
     var forgetModel : ForgetPassViewModel!
     var UploadTargetModel : UploadTargetViewModel!
     var GetTargetModel : GetTargetViewModel!
     var GetAllTargetModel : GetAllTargetViewModel!
    var DeleteTargetModel : DeleteTargetViewModel!
    var GetModelsModel : GetModelViewModel!
    var changePassModel : ChangePassViewModel!
    var TutorialModel : TutorialViewModel!
    var showProfileModel : ShowProfileViewModel!
    var updateProfileModel : UpdateProfileViewModel!
    var mapFriendModel: MapFriendViewModel!
     var user_id : Int = 0
    let Blurview = UIView()
    var refresh = UIRefreshControl()
    let actInd: UIImageView = UIImageView()
    var activityIndicator = UIActivityIndicatorView()
    var strLabel = UILabel()
    let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
     @IBOutlet weak var addTargetView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        user_id = defaults.object(forKey: "userid") as? Int ?? 0
        
    }
    
    
    func dotsRectangleView(){
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = #colorLiteral(red: 0.05945993215, green: 0.59061867, blue: 0.9574015737, alpha: 1)
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = addTargetView.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: addTargetView.bounds).cgPath
        addTargetView.layer.addSublayer(yourViewBorder)
    }
    
    func AddView(UiView: UIView){
        let BlurView = UIView()
        BlurView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
       // BlurView.layer.opacity = 0.4
        BlurView.center = view.center
        UiView.addSubview(BlurView)
    }
    
    func customAlert(_ title:String, _ messages:String)
    {
        let errorAlert = UIAlertController(title: title, message: messages, preferredStyle: .alert)
        let OK = UIAlertAction(title: "OK", style: .default, handler: nil)
        errorAlert.addAction(OK)
        let titleFont = [ NSAttributedString.Key.font : UIFont(name: "Roboto-Medium", size: 18)! ]
        let messageFont = [ NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 14)! ]
        let attributedTitle = NSMutableAttributedString(string: title, attributes: titleFont)
        let attributedMessage = NSMutableAttributedString(string: messages, attributes: messageFont)
        errorAlert.setValue(attributedTitle, forKey: "attributedTitle")
        errorAlert.setValue(attributedMessage, forKey: "attributedMessage")
        present(errorAlert, animated: true)
    }
    
    func AlertDismiss(title: String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
        self.dismiss(animated: true, completion: nil)
    }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func ChectInternet()-> Bool{
        if NetworkState.isConnected(){
            print("Internet Available")
            return true
        }
        else{
            customAlert("Sorry", "No Internet support")
            return false
        }
    }
   
    func showActivityIndicatory(uiView: UIView) {
        let image = GifImage.animatedImageWithGIFNamed(name: "8")
        Blurview.frame = uiView.frame
        Blurview.tag = 100
        Blurview.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.6)
        actInd.tag = 100
        actInd.contentMode = .scaleAspectFill
        actInd.clipsToBounds = true
        actInd.frame = CGRect(x: 0, y: 0, width: 75, height: 75)
        actInd.image = image
        actInd.backgroundColor = UIColor.clear
        actInd.center = uiView.center
        uiView.addSubview(Blurview)
        uiView.addSubview(actInd)
    }
    
    func HideActivityIndicator(uiView: UIView){
        Blurview.frame = uiView.frame
        Blurview.tag = 100
        Blurview.layer.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        let viewWithTag = uiView.viewWithTag(100)
        viewWithTag?.removeFromSuperview()
        let act = actInd.viewWithTag(100)
        act?.removeFromSuperview()
    }
    
    func activityIndicator(_ title: String) {
        
        strLabel.removeFromSuperview()
        activityIndicator.removeFromSuperview()
        effectView.removeFromSuperview()
        
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 160, height: 46))
        strLabel.text = title
        strLabel.font = .systemFont(ofSize: 14, weight: .medium)
        strLabel.textColor = UIColor(white: 0.9, alpha: 0.7)
        
        effectView.frame = CGRect(x: view.frame.midX - strLabel.frame.width/2, y: view.frame.midY - strLabel.frame.height/2 , width: 160, height: 46)
        effectView.layer.cornerRadius = 15
        effectView.layer.masksToBounds = true
        
        activityIndicator = UIActivityIndicatorView(style: .white)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityIndicator.startAnimating()
        effectView.contentView.addSubview(activityIndicator)
        effectView.contentView.addSubview(strLabel)
        view.addSubview(effectView)
    }
    
    @IBAction func backBtn(){
        dismiss(animated: true, completion: nil)
    }
    
    func applyCurvedPath(givenView: UIImageView,curvedPercent:CGFloat) {
        guard curvedPercent <= 1 && curvedPercent >= 0 else{
            return
        }
        
        let shapeLayer = CAShapeLayer(layer: givenView.layer)
        shapeLayer.path = self.pathCurvedForView(givenView: givenView,curvedPercent: curvedPercent).cgPath
        shapeLayer.frame = givenView.frame
        shapeLayer.masksToBounds = true
        givenView.layer.mask = shapeLayer
    }
    
    func pathCurvedForView(givenView: UIImageView, curvedPercent:CGFloat) ->UIBezierPath
    {
        let arrowPath = UIBezierPath()
        arrowPath.move(to: CGPoint(x:0, y:0))
        arrowPath.addLine(to: CGPoint(x:givenView.frame.size.width, y:0))
        arrowPath.addLine(to: CGPoint(x:givenView.frame.size.width, y:givenView.frame.size.height - (givenView.frame.size.height*curvedPercent)+75))
        arrowPath.addQuadCurve(to: CGPoint(x:0, y:givenView.frame.size.height - (givenView.frame.size.height*curvedPercent)+75), controlPoint: CGPoint(x:givenView.frame.size.width/2, y:givenView.frame.size.height + 40))
        arrowPath.addLine(to: CGPoint(x:0, y:0))
        arrowPath.close()
        
        return arrowPath
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

}

class NetworkState {
    class func isConnected() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
