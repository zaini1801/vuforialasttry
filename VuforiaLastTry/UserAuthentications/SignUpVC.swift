//
//  SignUpVC.swift
//  Shoclef
//
//  Created by user on 07/06/2018.
//  Copyright © 2018 Shoclef. All rights reserved.
//

import UIKit
import GoogleMaps

class SignUpVC: MainController, UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var locationManager : CLLocationManager!
    var lat : String = "33.6810"
    var lng : String = "73.9881"
    @IBOutlet weak var fnameView: UIView!
    @IBOutlet weak var lnameView: UIView!
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var passView: UIView!
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var emailView: UIView!
    
    @IBOutlet weak var ProfileImage: UIImageView!
    @IBOutlet weak var CoverImage: UIImageView!
    
    
    @IBOutlet weak var fnameLbl: UILabel!
    @IBOutlet weak var fnameTF: UITextField!
    
    @IBOutlet weak var lname: UILabel!
    @IBOutlet weak var lnameTF: UITextField!
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var usernameTF: UITextField!
    
    @IBOutlet weak var password: UILabel!
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var confirm: UILabel!
    @IBOutlet weak var CpasswordTF: UITextField!
    
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var emailTF: UITextField!
    
    var picker = UIImagePickerController()
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == fnameTF {
             let allowedCharacters = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharaterSet = CharacterSet(charactersIn: string)
            let alphabet =  allowedCharacterSet.isSuperset(of: typedCharaterSet)
            
            
            let Range = range.length + range.location > (fnameTF.text?.count)!
            
            if Range == false && alphabet == false {
                return false
            }
            
            
            let NewLength = (fnameTF.text?.count)! + string.count - range.length
            return NewLength <= 25
            
        } else if textField == lnameTF {
            let allowedCharacters = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharaterSet = CharacterSet(charactersIn: string)
            let alphabet =  allowedCharacterSet.isSuperset(of: typedCharaterSet)
            
            let Range = range.length + range.location > (lnameTF.text?.count)!
            
            if Range == false && alphabet == false {
                return false
            }
                
                
            let NewLength = (lnameTF.text?.count)! + string.count - range.length
            return NewLength <= 25
        } else if textField == usernameTF {
            if range.length + range.location > (usernameTF.text?.count)! {
                return false
            }
            let NewLength = (usernameTF.text?.count)! + string.count - range.length
            return NewLength <= 25
        } else if textField == passwordTF {
            if range.length + range.location > (passwordTF.text?.count)! {
                return false
            }
            let NewLength = (passwordTF.text?.count)! + string.count - range.length
            return NewLength <= 10
        } else if textField == CpasswordTF {
            if range.length + range.location > (CpasswordTF.text?.count)! {
                return false
            }
            let NewLength = (CpasswordTF.text?.count)! + string.count - range.length
            return NewLength <= 10
        }
        else{
            return true
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fnameTF.becomeFirstResponder()
        
        HideCard(card: fnameView)
        fnameTF.frame.size.height = 24
        fnameLbl.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        fnameTF.delegate = self
        lnameTF.delegate = self
        emailTF.delegate = self
        usernameTF.delegate = self
        passwordTF.delegate = self
        CpasswordTF.delegate = self
       
        ProfileImage.layer.cornerRadius = ProfileImage.frame.height/2
        fnameView.layer.shadowOpacity = 0.2
        fnameView.layer.shadowOffset.height = 1
        fnameView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        fnameTF.frame.size.height = 28
        fnameTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
        fnameLbl.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
        
        locationManager = CLLocationManager()
        locationManager.delegate = self as? CLLocationManagerDelegate
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                locationManager.requestWhenInUseAuthorization()
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        applyCurvedPath(givenView: CoverImage, curvedPercent: 0.35)
    }
    
    
    @IBAction func backBTN(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if fnameTF == textField{
            ShowCard(card: fnameView)
            HideCard(card: userView)
            HideCard(card: lnameView)
            HideCard(card: passView)
            HideCard(card: confirmView)
            HideCard(card: emailView)
            fnameTF.frame.size.height = 28
            fnameTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            fnameLbl.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
            textField.becomeFirstResponder()
        }
        if  textField == lnameTF{
            ShowCard(card: lnameView)
            HideCard(card: fnameView)
            HideCard(card: userView)
            HideCard(card: passView)
            HideCard(card: confirmView)
            HideCard(card: emailView)
            
            lnameTF.frame.size.height = 28
            lnameTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            lname.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
            textField.becomeFirstResponder()
        }
        if  textField == usernameTF{
            ShowCard(card: userView)
            HideCard(card: fnameView)
            HideCard(card: lnameView)
            HideCard(card: passView)
            HideCard(card: confirmView)
            HideCard(card: emailView)
            
            usernameTF.frame.size.height = 28
            usernameTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            username.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
            textField.becomeFirstResponder()
        }
        if textField == passwordTF{
            ShowCard(card: passView)
            HideCard(card: fnameView)
            HideCard(card: lnameView)
            HideCard(card: userView)
            HideCard(card: confirmView)
            passwordTF.frame.size.height = 28
            passwordTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            password.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
            textField.becomeFirstResponder()
        }
        
        if textField == CpasswordTF{
            ShowCard(card: confirmView)
            HideCard(card: fnameView)
            HideCard(card: lnameView)
            HideCard(card: userView)
            HideCard(card: passView)
            HideCard(card: emailView)
            
            CpasswordTF.frame.size.height = 28
            CpasswordTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            confirm.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
            textField.becomeFirstResponder()
        }
        if textField == emailTF{
            ShowCard(card: emailView)
            HideCard(card: fnameView)
            HideCard(card: lnameView)
            HideCard(card: userView)
            HideCard(card: passView)
            HideCard(card: confirmView)
            
            emailTF.frame.size.height = 28
            emailTF.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            email.textColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
            textField.becomeFirstResponder()
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == fnameTF{
            HideCard(card: fnameView)
            fnameTF.frame.size.height = 24
            fnameLbl.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if  textField == lnameTF{
            HideCard(card: lnameView)
            lnameTF.frame.size.height = 24
            lname.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if  textField == usernameTF{
            HideCard(card: userView)
            usernameTF.frame.size.height = 24
            username.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if textField == passwordTF{
            HideCard(card: passView)
            passwordTF.frame.size.height = 24
            password.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if textField == CpasswordTF{
            HideCard(card: confirmView)
            CpasswordTF.frame.size.height = 24
            confirm.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if textField == emailTF{
            HideCard(card: emailView)
            emailTF.frame.size.height = 24
            email.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == fnameTF{
            
            HideCard(card: fnameView)
            fnameTF.frame.size.height = 24
            fnameLbl.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if  textField == lnameTF {
            
            HideCard(card: lnameView)
            lnameTF.frame.size.height = 24
            lname.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if  textField == usernameTF {
            HideCard(card: usernameTF)
            usernameTF.frame.size.height = 24
            username.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if textField == passwordTF{
            HideCard(card: passView)
            passwordTF.frame.size.height = 24
            password.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if textField == CpasswordTF{
            HideCard(card: confirmView)
            CpasswordTF.frame.size.height = 24
            confirm.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
        if textField == emailTF{
            HideCard(card: emailView)
            emailTF.frame.size.height = 24
            email.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        }
    }
    
    func ShowCard(card: UIView)
    {
        card.layer.shadowOpacity = 0.2
        card.layer.shadowOffset.height = 1
        card.layer.cornerRadius = 15
        card.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func HideCard(card: UIView){
        card.layer.shadowOpacity = 0
        card.layer.shadowOffset.height = 0
        card.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
    }
    
    @IBAction func SubmitBtn(){
        let userEmail = emailTF.text?.trimmingCharacters(in: .whitespaces)
        resignFirstResponder()
        if (fnameTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            fnameTF.becomeFirstResponder()
            
            self.alertbox(msg: "First name is required.")
        }
        else  if (lnameTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            lnameTF.becomeFirstResponder()
            self.alertbox(msg: "Last name is required.")
            
        }
        else if (usernameTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            usernameTF.becomeFirstResponder()
            self.alertbox(msg: "Username is required.")
        }
        else if (passwordTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            passwordTF.becomeFirstResponder()
            self.alertbox(msg: "Password is required.")
        }
        else if (CpasswordTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            CpasswordTF.becomeFirstResponder()
            self.alertbox(msg: "Confirm password is required.")
        }
        else if (emailTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            emailTF.becomeFirstResponder()
            self.alertbox(msg: "Email is required.")
        }
        else if (cImage == nil) {
            
            self.alertbox(msg: "Please upload cover image.")
        }
        else if (pImage == nil) {
            
            self.alertbox(msg: "Please upload profile image.")
        }
        else if !isValidEmail(testStr: userEmail ?? "")
        {
            alertbox(msg: "Enter Valid Email...")
        }
        else{
            if ChectInternet() == true{
            if passwordTF.text == CpasswordTF.text {
                showActivityIndicatory(uiView: self.view)
                signUpModel = SignUPViewModel(fname: fnameTF.text ?? "".trimmingCharacters(in: .whitespaces), lname: lnameTF.text ?? "".trimmingCharacters(in: .whitespaces), username: usernameTF.text ?? "".trimmingCharacters(in: .whitespaces), password: passwordTF.text ?? "".trimmingCharacters(in: .whitespaces), email: emailTF.text ?? "".trimmingCharacters(in: .whitespaces), age: 0, country: "", state: "", city: "", zip: "", lat: lat, lng: lng, device_id: "", gender: "", profilePic: (ProfileImage.image?.convertBaser64Withoutsize(image: pImage ?? #imageLiteral(resourceName: "noimage")) ?? ""), coverPic: (CoverImage.image?.convertBaser64Withoutsize(image: cImage ?? #imageLiteral(resourceName: "noimage")) ?? ""))
                signUpModel.save { (bool, data) in
                    if bool == true{
                        self.HideActivityIndicator(uiView: self.view)
                        self.AlertDismiss(title: "Success", message: data)
                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC{
                            self.present(vc, animated: true, completion: nil)
                        }
                    }else{
                        self.HideActivityIndicator(uiView: self.view)
                        self.customAlert("Sorry", data)
                    }
                }
            }else  {
                passwordTF.becomeFirstResponder()
                CpasswordTF.becomeFirstResponder()
                self.alertbox(msg: "Password And Confirm Password Are Not Same!")
            }
             }
        }
    }

    func alertbox(msg: String){
        let alert = UIAlertController(title: "Warning", message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "Okay", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func CoverBtn(_ sender: UIBarButtonItem) {
        pic = "cover"
        let alert = UIAlertController(title: "Choose Option", message: "Upload from libray or Capture from camera", preferredStyle: .actionSheet)
        let Add = UIAlertAction(title: "Camera", style: .default, handler: {
            UIAlertAction in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker.delegate = self
                self.picker.allowsEditing = false
                self.picker.sourceType = .camera
                self.present(self.picker, animated: true, completion: nil)
            }
        })
        
        let load = UIAlertAction(title: "Upload Photos", style: .default, handler: {UIAlertAction in
            self.picker.delegate = self
            self.picker.allowsEditing = false
            self.picker.sourceType = .photoLibrary
            self.present(self.picker, animated: true, completion: nil)
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
      //  Add.setValue(UIImage(named: "camera@3x.png")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
      //  load.setValue(UIImage(named: "gallery@3x.png")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
        alert.addAction(load)
        alert.addAction(Add)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func ProfileBtn(_ sender: UIBarButtonItem) {
        pic = "profile"
        let alert = UIAlertController(title: "Choose Option", message: "Upload from libray or Capture from camera", preferredStyle: .actionSheet)
        let Add = UIAlertAction(title: "Camera", style: .default, handler: {
            UIAlertAction in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker.delegate = self
                self.picker.allowsEditing = true
                self.picker.sourceType = UIImagePickerController.SourceType.camera
                self.present(self.picker, animated: true, completion: nil)
            }
        })
        
        let load = UIAlertAction(title: "Upload Photos", style: .default, handler: {UIAlertAction in
            self.picker.delegate = self
            self.picker.allowsEditing = true
            self.picker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(self.picker, animated: true, completion: nil)
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
     //   Add.setValue(UIImage(named: "camera@3x.png")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
      //  load.setValue(UIImage(named: "gallery@3x.png")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
        alert.addAction(load)
        alert.addAction(Add)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    var pic = "cover"
    var pImage : UIImage? = nil
    var cImage : UIImage? = nil
    
    func imagePickerController(_ picker: UIImagePickerController,
    didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        if let pickedImage = info[.originalImage] as? UIImage{
            if pic == "cover"{
                CoverImage.contentMode = .scaleAspectFill
                CoverImage.image = pickedImage
                cImage = pickedImage
            }
            else{
                ProfileImage.contentMode = .scaleAspectFill
                ProfileImage.image = pickedImage
                pImage = pickedImage
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension SignUpVC{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last!
        lat = String(location.coordinate.latitude)
        lng = String(location.coordinate.longitude)
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            print(uuid)
        }
        locationManager.stopUpdatingLocation()
    }
}
