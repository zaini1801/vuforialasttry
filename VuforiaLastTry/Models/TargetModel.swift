//
//  TargetModel.swift
//  Interactive Wall
//
//  Created by user on 17/01/2019.
//  Copyright © 2019 Nomi Malik. All rights reserved.
//

import Foundation
import Moya
import UIKit

class UploadTargetViewModel{
    var user_id : Int!
    var texture : [Moya.MultipartFormData]!
    var model :   [Moya.MultipartFormData]!
    var data :    [Moya.MultipartFormData]!
    
    init(data: [Moya.MultipartFormData]){
        self.data = data
    }
    
    func UploadData(completion: @escaping(Bool)->()){
        let target = UploadTarget(ut: self)
        PHPDataProvide.request(PHPServiceData.uploadTexture(target.data)) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(UploadTargerResult.self, from: result.data)
                    if json.status == "success"{
                        completion(true)
                    }else{
                        completion(false)
                    }
                }catch{
                     completion(false)
                }
            case .failure(_):
                 completion(false)
            }
        }
    }
}


class GetTargetViewModel{
    var user_id : Int!
   // var cloud_id : String!
   
    init(user_id: Int) {
        self.user_id = user_id
     //   self.cloud_id = cloud_id
    }
    
    func GetTargetData(completion: @escaping(Bool,[GetTargetResultElement])->()){
        let target = GetTarget(gt: self)
        PHPDataProvide.request(PHPServiceData.scanTexture(target.user_id)) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(GetTargetResult.self, from: result.data)
                    if json.count != 0{
                        completion(true,json)
                    }else{
                        completion(false,[])
                    }
                }catch{
                    completion(false,[])
                }
            case .failure(_):
                completion(false,[])
            }
        }
    }
}

class GetAllTargetViewModel{
    var user_id : Int!
   
    
    init(user_id: Int) {
        self.user_id = user_id
    }
    
    func GetTargetData(completion: @escaping(Bool,[GetAllTargetResultElement])->()){
        let target = GetAllTarget(gt: self)
        PHPDataProvide.request(PHPServiceData.GetAllTexture(target.user_id)) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(GetAllTargetResult.self, from: result.data)
                    if json.count != 0{
                        completion(true,json)
                    }else{
                        completion(false,[])
                    }
                }catch{
                    completion(false,[])
                }
            case .failure(_):
                completion(false,[])
            }
        }
    }
    
}

class DeleteTargetViewModel{
    var Target_Id: Int!
    
    init(Target_Id: Int) {
        self.Target_Id = Target_Id
    }
    
    func DeleteTargetData(completion: @escaping(Bool)->()){
        let target = DeleteTarget(dt: self)
        PHPDataProvide.request(PHPServiceData.deleteTarget(target.Target_Id)) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(UploadTargerResult.self, from: result.data)
                    if json.status == "success"{
                        completion(true)
                    }else{
                        completion(false)
                    }
                }catch{
                    completion(false)
                }
            case .failure(_):
                completion(false)
            }
        }
    }
}
