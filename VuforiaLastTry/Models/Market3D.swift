//
//  Market3D.swift
//  VuforiaLastTry
//
//  Created by user on 22/01/2019.
//  Copyright © 2019 Beliverz. All rights reserved.
//

import Foundation

class GetModelViewModel{
   
    func GetModelsData(completion: @escaping(Bool,[GetModelResult])->()){
    MarketDataProvide.request(MarketServiceData.GetModels()) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(GetModelResults.self, from: result.data)
                    if json.count != 0{
                        completion(true,json)
                    }else{
                        completion(false,[])
                    }
                }catch{
                    completion(false,[])
                }
            case .failure(_):
                completion(false,[])
            }
        }
    }
}
