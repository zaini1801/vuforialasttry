//
//  MapInfoModel.swift
//  VuforiaLastTry
//
//  Created by user on 24/01/2019.
//  Copyright © 2019 Beliverz. All rights reserved.
//

import Foundation

class MapFriendViewModel {
   
        var user_id: Int!
        var tag : String!
        var lat : String!
        var lng : String!
        
    init(user_id: Int, tag: String, lat: String, lng: String) {
        self.user_id = user_id
        self.tag = tag
        self.lat = lat
        self.lng = lng
    }
    
    func GetMapFriend(completion:@escaping(Bool,[AroundMeResultDatum])->()){
        let user = MapFriend(mf: self)
        print(user.user_id ?? 0)
        print(user.lat ?? "")
        print(user.lng ?? "")
        print(user.tag ?? "")
        CMSDataProvide.request(CMSServiceData.MapOnFriend(user_id: user.user_id, tag: user.tag, lat: user.lat, lng: user.lng)) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(AroundMe.self, from: result.data)
                    if json.aroundMeResult?.status == "success"{
                        completion(true,(json.aroundMeResult?.data)!)
                    }else{
                        completion(false,[])
                    }
                }catch{
                    completion(false,[])
                }
            case .failure(_):
                completion(false,[])
            }
        }
    }
}
