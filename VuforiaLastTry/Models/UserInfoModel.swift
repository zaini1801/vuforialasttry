//
//  UserInfo.swift
//  Interactive Wall
//
//  Created by user on 16/01/2019.
//  Copyright © 2019 Nomi Malik. All rights reserved.
//

import Foundation
import UIKit

class LoginViewModel{
    var username: String!
    var password: String!
    
    init(username: String, password: String){
    self.username = username
    self.password = password
    }
    
    func SignIn(completion:@escaping (Bool,[LoginData]) -> ()){
        let user = Login(lm: self)
        CMSDataProvide.request(CMSServiceData.login(user.username, user.password)) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(LoginResult.self, from: result.data)
                    if json.loginResult?.status == "success" {
                        completion(true,(json.loginResult?.data)!)
                    }
                    else{
                        completion(false, [])
                    }
                }catch{
                    completion(false,[] )
                }
            case .failure(_):
                completion(false, [])
            }
        }
    }
}

class SignUPViewModel{
    var fname : String!
    var lname : String!
    var username : String!
    var password : String!
    var email : String!
    var age : Int!
    var country : String!
    var state : String!
    var city : String!
    var zip : String!
    var lat : String!
    var lng : String!
    var device_id : String!
    var gender : String!
    var profilePic : String!
    var coverPic : String!
    
    init(fname: String, lname: String, username: String, password: String, email: String,age: Int, country: String, state: String, city: String,zip:String, lat: String, lng: String, device_id:String,gender:String,profilePic:String,coverPic: String) {
        self.fname = fname
        self.lname = lname
        self.username = username
        self.password = password
        self.email = email
        self.age = age
        self.country = country
        self.state = state
        self.city = city
        self.zip = zip
        self.lat = lat
        self.lng = lng
        self.device_id = device_id
        self.gender = gender
        self.profilePic = profilePic
        self.coverPic = coverPic
    }
    
    func save(completion:@escaping (Bool,String) -> ()){
        let user = SignUp(sm: self)
        print(user.fname)
        print(user.lname)
        print(user.username)
        print(user.password)
        print(user.email)
        print(user.lat)
        print(user.lng)
        CMSDataProvide.request(CMSServiceData.SignUP(user.fname, user.lname, user.username, user.password, user.email, 0, "", "","", "", user.lat, user.lng, "", "", user.profilePic, user.coverPic)) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(SignupResult.self, from: result.data)
                    if json.signupResult?.status == "success" {
                    completion(true,json.signupResult?.message ?? "")
                    }
                    else{
                        completion(false, json.signupResult?.message ?? "")
                    }
                }catch{
                    completion(false,"Not Registered")
                }
            case .failure(_):
                completion(false, "Something went wrong, Try Again")
            }
        }
    }
}

class ChangePassViewModel{
    var user_id : Int!
    var oldPass : String!
    var newPass : String!
    
    init(user_id: Int, oldPass: String, newPass: String) {
        self.user_id = user_id
        self.oldPass = oldPass
        self.newPass = newPass
    }
    
    func ChangePass(completion:@escaping(Bool,String)->()){
        let forget = ChangePassword(cp: self)
        CMSDataProvide.request(CMSServiceData.ChangePass(forget.user_id, forget.oldPass, forget.newPass)) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(PasswordData.self, from: result.data)
                    if json.changePasswordResult.status == "success" {
                    completion(true,json.changePasswordResult.message ?? "")
                    }
                    else{
                    completion(false, json.changePasswordResult.message ?? "")
                    }
                }catch{
                    completion(false, "Something went wrong, Try Again")
                }
            case .failure(_):
                completion(false, "Something went wrong, Try Again")
            }
        }
    }
    
}

class ForgetPassViewModel{
    var email : String!
    
    init(email: String) {
        self.email = email
    }
    
    func forgetPassword(completion:@escaping(Bool,String)->()){
        let forget = ForgetPassword(fp: self)
        CMSDataProvide.request(CMSServiceData.ForgetPass(forget.email)) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(ForgetPasswordResult.self, from: result.data)
                    if json.forgetPasswordResult?.status == "success" {
                    completion(true,json.forgetPasswordResult?.message ?? "")
                    }
                    else{
                        completion(false, json.forgetPasswordResult?.message ?? "")
                    }
                }catch{
                    completion(false, "Might be the email you entered is wrong, Try Again")
                }
            case .failure(_):
                completion(false, "Might be the email you entered is wrong, Try Again")
            }
        }
    }
}

class TutorialViewModel{
    func Tutorials(completion:@escaping(Bool,[VideoTutorial])->()){
        
        CMSDataProvide.request(CMSServiceData.Tutorials()) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(TutorialResult.self, from: result.data)
                    if json.vedioTutorialDataResult?.status == "success" {
                        completion(true,(json.vedioTutorialDataResult?.data!)!)
                    }
                    else{
                        completion(false, [])
                    }
                }catch{
                    completion(false, [])
                }
            case .failure(_):
                completion(false,[])
            }
        }
    }
    
    func howToUse(completion:@escaping(Bool,[Datum])->()){
        
        CMSDataProvide.request(CMSServiceData.howtoUse()) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(HowToUseResult.self, from: result.data)
                    if json.tutorialsResult?.status == "success" {
                        completion(true,(json.tutorialsResult?.data)!)
                    }
                    else{
                        completion(false, [])
                    }
                }catch{
                    completion(false, [])
                }
            case .failure(_):
                completion(false,[])
            }
        }
    }
    
    func PrivacyPolicy(completion:@escaping(Bool,[PrivacyDatum])->()){
        
        CMSDataProvide.request(CMSServiceData.Policy()) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(PolicyDataResult.self, from: result.data)
                    if json.policyDataResult?.status == "success" {
                        completion(true,(json.policyDataResult?.data)!)
                    }
                    else{
                        completion(false, [])
                    }
                }catch{
                    completion(false, [])
                }
            case .failure(_):
                completion(false,[])
            }
        }
    }
    
}

class UpdateProfileViewModel{
    
    var fname : String!
    var lname : String!
    var coverPic : String!
    var profilePic : String!
    var about : String!
    var phone : String!
    var age : Int!
    var userid : Int!
    
    init(fname: String, lname: String, coverImage : String, profileImage : String,about : String, phone : String, age: Int, user_id: Int) {
        self.fname = fname
        self.lname = lname
        self.coverPic = coverImage
        self.profilePic = profileImage
        self.about = about
        self.phone = phone
        self.age = age
        self.userid = user_id
    }
    
    func updateProfile(completion:@escaping(Bool,String)->()){
        let update = UpdateProfile(up: self)
        CMSDataProvide.request(CMSServiceData.UpdateProfile(update.fname, update.lname, update.coverPic, update.profilePic, update.about, update.phone, update.age, update.userid)) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(UpdateProfileResult.self, from: result.data)
                    
                    if json.updateResult.status == "success"{
                    completion(true,json.updateResult.Message ?? "")
                    }
                    else{
                        completion(false,json.updateResult.Message ?? "")
                    }
                }catch{
                    completion(false, "Something went wrong..")
                }
            case .failure(_):
                completion(false,"Anything happened wrong, Try again..")
            }
        }
    }
}

class ShowProfileViewModel{
    var user_id : Int!
    var profile_id : Int!
    
    init(user_id: Int, profile_id:Int) {
        self.user_id = user_id
        self.profile_id = profile_id
    }
    
    func showProfile(completion:@escaping (Bool,[ShowProfileDatum]) -> ()){
        let user = ShowProfile(sp: self)
        CMSDataProvide.request(CMSServiceData.ShowProfile(user.user_id, user.profile_id)) { (data) in
            switch data{
            case .success(let result):
                do{
                    let json = try JSONDecoder().decode(ShowData.self, from: result.data)
                    if json.showProfileResult.status == "success" {
                        completion(true,(json.showProfileResult.data)!)
                    }
                    else{
                        completion(false, [])
                    }
                }catch{
                    completion(false,[] )
                }
            case .failure(_):
                completion(false, [])
            }
        }
    }
}
