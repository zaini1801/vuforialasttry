//
//  MapController.swift
//  VuforiaLastTry
//
//  Created by user on 23/01/2019.
//  Copyright © 2019 Beliverz. All rights reserved.
//

import UIKit
import GoogleMaps

class MapController: MainController, GMSMapViewDelegate,GMUClusterManagerDelegate, CLLocationManagerDelegate {

     @IBOutlet weak var MapView: GMSMapView!
     private var clusterManager: GMUClusterManager!
     var locationManager : CLLocationManager!
    var latitude = String()
    var longitude = String()
    @IBOutlet weak var optionView: UIView!
    var mapData = [AroundMeResultDatum]()
     override func viewDidLoad() {
        super.viewDidLoad()

        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        
        MapView.addSubview(optionView)
        MapView.delegate = self
        self.MapView?.isMyLocationEnabled = true
        MapView.settings.myLocationButton = true
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: MapView,clusterIconGenerator: iconGenerator)
        clusterManager = GMUClusterManager(map: MapView, algorithm: algorithm,renderer: renderer)
        
       // generateClusterItems()
        clusterManager.cluster()
        clusterManager.setDelegate(self, mapDelegate: self)
    }
    
    private func generateClusterItems() {
        for index in 0..<mapData.count {
            let lat = Double(mapData[index].lat ?? "")
            let lng = Double(mapData[index].lng ?? "")
            let name = mapData[index].name ?? ""
            let item =
                POIItem(position: CLLocationCoordinate2DMake(lat ?? 0, lng ?? 0), name: name)
            clusterManager.add(item)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                locationManager.requestWhenInUseAuthorization()
            case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.distanceFilter = 50
                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    func createMarker(latitude: Double, longitude: Double, zoom: Float){
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: zoom)
        self.MapView.camera = camera
    }
    
    func getMapUser(user_id: Int,tag: String, lat: String, lng: String,zoom: Float, completion: @escaping()->()){
        if ChectInternet() == true{
            showActivityIndicatory(uiView: self.view)
            mapFriendModel = MapFriendViewModel(user_id: user_id, tag: tag, lat: lat , lng: lng)
            mapFriendModel.GetMapFriend { (bool, data) in
                if bool == true{
                    if data.count != 0{
                        self.mapData = data
                        DispatchQueue.main.async{
                      //  for i in 0..<data.count {
                            //self.DynamicMarker(lat: Double(data[i].lat ?? "0") ?? 0.0 , lng: Double(data[i].lng ?? "0") ?? 0.0, name: data[i].name ?? "No Name", url:  "https://shoclefapi.shoclef.com" + (data[i].profileImage ?? ""), zoom: Double(zoom), userid: "\(data[i].userID!)", address:data[i].address ?? "No Address")
                      //  }
                         self.generateClusterItems()
                        completion()
                        }
                    }
                }else{
                    self.HideActivityIndicator(uiView: self.view)
                    self.customAlert("Sorry", " No Data found at this time")
                    completion()
                }
            }
        }
    }
    
    
    func DynamicMarker(lat: Double, lng: Double, name: String, url: String, zoom: Double,userid: String,address: String){
        DispatchQueue.main.async {
            let marker = GMSMarker()
            let DynamicView = UIView(frame: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 50, height: 50)))
            DynamicView.backgroundColor=UIColor.clear
            var imageViewForPinMarker : UIImageView
            
            imageViewForPinMarker  = UIImageView(frame:CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 50, height: 50)))
            imageViewForPinMarker.clipsToBounds = true
            imageViewForPinMarker.layer.cornerRadius = imageViewForPinMarker.frame.height/2
            imageViewForPinMarker.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            imageViewForPinMarker.layer.borderWidth = 2
            imageViewForPinMarker.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
           // imageViewForPinMarker.
            //sd_setImage(with: URL(string: url), placeholderImage: placeHolderImage)
            DynamicView.addSubview(imageViewForPinMarker)
        UIGraphicsBeginImageContextWithOptions(DynamicView.frame.size, false, UIScreen.main.scale)
            DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
            let imageConverted: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            marker.position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            marker.title = name
            marker.snippet = address
            marker.accessibilityHint = userid
            marker.icon = imageConverted
            marker.map = self.MapView
            self.createMarker(latitude: lat, longitude: lng, zoom: Float(zoom))
        }
    }
    
    
    @IBAction func optionBtn(){
        let actionsheet = UIAlertController(title: "Choose view of map", message: "", preferredStyle: .actionSheet)
        actionsheet.addAction(UIAlertAction(title: "World view", style: .default, handler: { (_) in
            self.getMapUser(user_id: self.user_id, tag: "latlng", lat: self.latitude, lng: self.longitude, zoom: 15) {
                self.HideActivityIndicator(uiView: self.view)
            }
        }))
        actionsheet.addAction(UIAlertAction(title: "Country view", style: .default, handler: { (_) in
            self.getMapUser(user_id: self.user_id, tag: "country", lat: self.latitude, lng: self.longitude, zoom: 8) {
                self.HideActivityIndicator(uiView: self.view)
            }
        }))
        actionsheet.addAction(UIAlertAction(title: "City view", style: .default, handler: { (_) in
            self.getMapUser(user_id: self.user_id, tag: "city", lat: self.latitude, lng: self.longitude, zoom: 13) {
                self.HideActivityIndicator(uiView: self.view)
            }
        }))
        actionsheet.addAction(UIAlertAction(title: "NearBy", style: .default, handler: { (_) in
            self.getMapUser(user_id: self.user_id, tag: "distance", lat: self.latitude, lng: self.longitude, zoom: 13) {
                self.HideActivityIndicator(uiView: self.view)
            }
        }))
        actionsheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(actionsheet, animated: true, completion:   nil)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("Marker tapped")
        if let poiItem = marker.userData as? POIItem {
            NSLog("Did tap marker for cluster item \(String(describing: poiItem.name))")
        } else {
            NSLog("Did tap a normal marker")
        }
        return true
    }
}
extension MapController{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last!
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 15.0)
        latitude = String(location.coordinate.latitude)
        longitude = String(location.coordinate.longitude)
        MapView.camera = camera
        locationManager.stopUpdatingLocation()
        getMapUser(user_id: user_id, tag: "latlng", lat: latitude, lng: longitude, zoom: 15) {
            self.HideActivityIndicator(uiView: self.view)
        }
    }
    
    func clusterManager(clusterManager: GMUClusterManager, didTapCluster cluster: GMUCluster) {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position, zoom: MapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        MapView.moveCamera(update)
    }
    
    class POIItem: NSObject, GMUClusterItem {
        var position: CLLocationCoordinate2D
        var name: String!
        
        init(position: CLLocationCoordinate2D, name: String) {
            self.position = position
            self.name = name
//            let DynamicView = UIView(frame: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 50, height: 50)))
//            DynamicView.backgroundColor = UIColor.clear
//            var imageViewForPinMarker : UIImageView
//
//            imageViewForPinMarker  = UIImageView(frame:CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 50, height: 50)))
//            imageViewForPinMarker.clipsToBounds = true
//            imageViewForPinMarker.layer.cornerRadius = imageViewForPinMarker.frame.height/2
//            imageViewForPinMarker.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//            imageViewForPinMarker.layer.borderWidth = 2
//            imageViewForPinMarker.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
//            // imageViewForPinMarker.
//            //sd_setImage(with: URL(string: url), placeholderImage: placeHolderImage)
//            DynamicView.addSubview(imageViewForPinMarker)
//            UIGraphicsBeginImageContextWithOptions(DynamicView.frame.size, false, UIScreen.main.scale)
//            DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
//            let imageConverted: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
//            UIGraphicsEndImageContext()
//            let marker = GMSMarker()
//            marker.position = position
//            marker.title = name
//            // marker.snippet = address
//            marker.icon = imageConverted
//            marker.map = MapView
        }
    }
}


