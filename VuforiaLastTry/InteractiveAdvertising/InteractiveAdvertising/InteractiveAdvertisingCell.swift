//
//  InteractiveAdvertisingCell.swift
//  Interactive Wall
//
//  Created by apple on 25/10/2018.
//  Copyright © 2018 Nomi Malik. All rights reserved.
//

import UIKit
import MediaPlayer
import MarqueeLabel
class InteractiveAdvertisingCell: UITableViewCell {
    
    
    @IBOutlet var leftImgShow: UIImageView!
    
    @IBOutlet var NameLbl: MarqueeLabel!
    
    @IBOutlet var timeLbl: MarqueeLabel!
    
    var moviePlayer:MPMoviePlayerController!
    
    @IBOutlet var middleImgShow: UIImageView!
    @IBOutlet var SelectButton: UIButton!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var ModelView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        leftImgShow.layer.cornerRadius = leftImgShow.frame.height/2
        middleImgShow.layer.cornerRadius = middleImgShow.frame.height/2
       ModelView.layer.cornerRadius = ModelView.frame.height/2
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        timeLbl.text = ""
        leftImgShow.image = nil
        middleImgShow.image = nil
    
    }
}
