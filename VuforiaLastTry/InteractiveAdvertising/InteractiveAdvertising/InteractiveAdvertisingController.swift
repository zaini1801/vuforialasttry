//
//  InteractiveAdvertisingController.swift
//  Interactive Wall
//
//  Created by apple on 25/10/2018.
//  Copyright © 2018 Nomi Malik. All rights reserved.
//


import UIKit
import Kingfisher
import MediaPlayer
import AVKit

class InteractiveAdvertisingController: MainController, UITableViewDelegate, UITableViewDataSource {
    
     @IBOutlet var TableViewIA: UITableView!
    var data = [GetAllTargetResultElement]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  let url = URL(string: "https://ar.shoclef.com/Admin_Panel/ModVidImg/Model_2019-01-21-13-30-12.mp4")
      //  let file = url?.pathExtension
       // print(file)
       // moviePlayer = MPMoviePlayerController(contentURL: url)
      //  moviePlayer.view.frame = CGRect(x: 200, y: 200, width: 100, height: 100)
      //  moviePlayer.isFullscreen = true
      //  moviePlayer.controlStyle = .embedded
      //  moviePlayer.prepareToPlay()
      //  moviePlayer.play()
       // self.view.addSubview(moviePlayer.view)
        TableViewIA.addSubview(refresh)
        refresh.addTarget(self, action: #selector(ReloadData), for: .touchUpInside)
        ReloadData()
    }
    
    @objc func ReloadData(){
        GetAllTargets {
            self.refresh.endRefreshing()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InteractiveAdvertisingCell", for: indexPath) as! InteractiveAdvertisingCell
       // cell.NameLbl.text = data[indexPath.row].modelName
         cell.leftImgShow.kf.setImage(with: URL(string: imageUrl + (data[indexPath.row].targetPath ?? "")))
        if let url = URL(string: imageUrl + (data[indexPath.row].modelPath ?? "")) {
            let fileExtension = url.pathExtension
            print(fileExtension)
            if fileExtension == "mp4"{
                cell.moviePlayer = MPMoviePlayerController(contentURL: url as URL)
                cell.middleImgShow.image = #imageLiteral(resourceName: "play")
                cell.moviePlayer.view.frame = cell.middleImgShow.frame
                cell.moviePlayer.isFullscreen = true
                cell.moviePlayer.controlStyle = .embedded
                //cell.moviePlayer.prepareToPlay()
               // cell.moviePlayer.play()
                cell.ModelView.addSubview(cell.moviePlayer.view)
            }else{
                cell.middleImgShow.kf.setImage(with: URL(string: imageUrl + (data[indexPath.row].modelPath ?? "")))
            }
        }
        cell.timeLbl.text = data[indexPath.row].createdDate ?? "Jan 15, 2018"
        cell.SelectButton.addTarget(self, action: #selector(SelectBtn(sender:)), for: .touchUpInside)
        cell.NameLbl.text = data[indexPath.row].targetName ?? ""
        let tapGesture = UITapGestureRecognizer (target: self, action: #selector(imgTap(tapGesture:)))
        cell.leftImgShow.addGestureRecognizer(tapGesture)
        cell.SelectButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(deleteBtn), for: .touchUpInside)
        cell.deleteButton.tag = indexPath.row
        return cell
    }

    @objc func imgTap(tapGesture: UITapGestureRecognizer) {
        
        let imgView = tapGesture.view as! UIImageView
        let newImageView = UIImageView(image: imgView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage(sender:)))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
    }
    
    
    @objc func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    @objc func SelectBtn(sender: UIButton){
        let index = IndexPath(row: sender.tag, section: 0)
        if let cell = TableViewIA.cellForRow(at: index) as? InteractiveAdvertisingCell{
            let url = URL(string: imageUrl + (data[index.row].modelPath ?? ""))
            if url?.pathExtension == "jpg"{
                let newImageView = UIImageView(image: cell.middleImgShow.image)
                newImageView.frame = UIScreen.main.bounds
                newImageView.backgroundColor = .black
                newImageView.contentMode = .scaleAspectFit
                newImageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage(sender:)))
                newImageView.addGestureRecognizer(tap)
                self.view.addSubview(newImageView)
            }else{
               // cell.moviePlayer = MPMoviePlayerController(contentURL: url)
                cell.moviePlayer.isFullscreen = true
                cell.moviePlayer.prepareToPlay()
                cell.moviePlayer.play()
            }
        }
    }
    
    
    @objc func deleteBtn(sender: UIButton){
        let index = IndexPath(row: sender.tag, section: 0)
        DeleteTarget(target_id: data[sender.tag].targetID ?? 0, index: index)
    }
    
    func DeleteTarget(target_id: Int, index: IndexPath){
        if ChectInternet() == true{
            showActivityIndicatory(uiView: self.view)
            DeleteTargetModel = DeleteTargetViewModel(Target_Id: target_id)
            DeleteTargetModel.DeleteTargetData { (bool) in
                if bool == true{
                    self.data.remove(at: index.row)
                    self.TableViewIA.deleteRows(at: [index], with: .fade)
                    self.HideActivityIndicator(uiView: self.view)
                }else{
                    self.HideActivityIndicator(uiView: self.view)
                    self.customAlert("Sorry", "Couldn't delete")
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func GetAllTargets(completion: @escaping()->()){
        if ChectInternet() == true{
           showActivityIndicatory(uiView: self.view)
            GetAllTargetModel = GetAllTargetViewModel(user_id: self.user_id)
            GetAllTargetModel.GetTargetData { (bool, data) in
                if bool == true{
                    self.data = data
                    self.TableViewIA.reloadData()
                    self.refresh.endRefreshing()
                    self.HideActivityIndicator(uiView: self.view)
                    completion()
                }else{
                self.HideActivityIndicator(uiView: self.view)
                self.AlertDismiss(title: "Sorry", message: "No Data Found")
                }
            }
        }
    }
    
    func createThumbnailOfVideoFromRemoteUrl(url: String) -> UIImage? {
        let asset = AVAsset(url: URL(string: url)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        //Can set this to improve performance if target size is known before hand
        //assetImgGenerate.maximumSize = CGSize(width,height)
        let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}
