//
//  ScanController.swift
//  VuforiaLastTry
//
//  Created by user on 29/01/2019.
//  Copyright © 2019 Beliverz. All rights reserved.
//

import UIKit
import ARKit
import SceneKit.ModelIO
import Alamofire

class ScanController: MainController {

    @IBOutlet weak var activity:UIActivityIndicatorView!
    @IBOutlet weak var LoadingLbl:UILabel!
    @IBOutlet weak var AssetView:UIView!
    @IBOutlet weak var sceneView: ARSCNView!
    let fadeDuration: TimeInterval = 0.3
    let rotateDuration: TimeInterval = 3
    let waitDuration: TimeInterval = 0.5
    let delegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
    lazy var fadeAndSpinAction: SCNAction = {
        let rotate = SCNAction.rotateBy(x: 0, y: 0, z: CGFloat.pi * 360 / 180, duration: rotateDuration)
        return .sequence([
            .fadeIn(duration: fadeDuration),
            .rotateBy(x: 0, y: 0, z: CGFloat.pi * 360 / 180, duration: rotateDuration),
            .repeatForever(rotate),
            .wait(duration:  waitDuration),
            .fadeOut(duration: fadeDuration)
            ])
    }()
    
    lazy var fadeAction: SCNAction = {
        return .sequence([
            .fadeOpacity(by: 0.8, duration: fadeDuration),
            .wait(duration: waitDuration),
            .fadeOut(duration: fadeDuration)
            ])
    }()
    
    lazy var treeNode: SCNNode = {
        guard let scene = SCNScene(named: "wheels.scn"),
            let node = scene.rootNode.childNode(withName: "wheel", recursively: false) else { return SCNNode() }
        let scaleFactor = 0.005
        node.scale = SCNVector3(scaleFactor, scaleFactor, scaleFactor)
        node.eulerAngles.x = -.pi / 2
        return node
    }()
    
    lazy var bookNode: SCNNode = {
        let url = URL(string: "http://ar.shoclef.com/Admin_Panel/ModVidImg/Model_2019-01-22-20-54-59.mp4")
        let mp4URL = url?.deletingPathExtension().appendingPathExtension("mp4")
         let currentFrame = self.sceneView.session.currentFrame
        
        
        let videoPlayer = AVPlayer(url: mp4URL!)
        
        let videoNode = SKVideoNode(avPlayer: videoPlayer)
        videoNode.play()
        
        let skScene = SKScene(size: CGSize(width: 300, height: 400))
        skScene.addChild(videoNode)
        
        videoNode.position = CGPoint(x: skScene.size.width/2, y: skScene.size.height/2)
        videoNode.size = skScene.size
        
        let tvPlane = SCNPlane(width: 1.0, height: 0.75)
        tvPlane.firstMaterial?.diffuse.contents = skScene
        tvPlane.firstMaterial?.isDoubleSided = true
        
        var tvPlaneNode = SCNNode(geometry: tvPlane)
        tvPlaneNode.opacity = 0
        tvPlaneNode.position.y = 0.2
        // tvPlaneNode.runAction(self.fadeAndSpinAction)
        var translation = matrix_identity_float4x4
        translation.columns.3.z = -1.0
        tvPlaneNode.simdTransform = matrix_multiply((currentFrame?.camera.transform)!, translation)
        tvPlaneNode.eulerAngles = SCNVector3(Double.pi,0.1,0.1)
        let scene = SCNScene()
        guard var node = scene.rootNode.childNode(withName: "Model_2019-01-22-20-54-59.mp4", recursively: false) else { return SCNNode() }
        let scaleFactor  = 0.1
        node.scale = SCNVector3(scaleFactor, scaleFactor, scaleFactor)
        node = tvPlaneNode
        return node
    }()
    
    lazy var mountainNode: SCNNode = {
        guard let scene = SCNScene(named: "mountain.scn"),
            let node = scene.rootNode.childNode(withName: "mountain", recursively: false) else { return SCNNode() }
        let scaleFactor  = 0.25
        node.scale = SCNVector3(scaleFactor, scaleFactor, scaleFactor)
        node.eulerAngles.x += -.pi / 2
        return node
    }()
    
    var Node = SCNNode()
    override func viewDidLoad() {
        super.viewDidLoad()
        sceneView.delegate = self
        configureLighting()
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveData, object: nil)
        let configuration = ARWorldTrackingConfiguration()
        let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
        self.sceneView.session.run(configuration, options: options)
    }
    
    
    func configureLighting() {
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // GetTarget()
        LoadingLbl.text = "\(String(describing: delegate?.typeArray.count ?? 0))/\(String(describing: delegate?.total ?? 0))"
        if LoadingLbl.text == "\(String(describing: delegate?.total ?? 0))/\(String(describing: delegate?.total ?? 0))"{
                self.AssetView.isHidden = true
            }
        if let ref = delegate?.newReferenceImages{
            resetTrackingConfiguration(detectImage: ref)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    @IBAction func resetButtonDidTouch(_ sender: UIBarButtonItem) {
        if (delegate?.total ?? 0) == 0{
            delegate?.GetTarget()
            AssetView.isHidden = false
        }else{
            customAlert("Server Alert", "Assets are already loaded")
            print(delegate?.newReferenceImages.count ?? 0)
        }
    }
    
    var counter : Int = 0
    @objc func onDidReceiveData(_ notification:Notification) {
        LoadingLbl.text = "\(String(describing: delegate?.typeArray.count ?? 0))/\(String(describing: delegate?.total ?? 0))"
        if LoadingLbl.text == "\(String(describing: delegate?.total ?? 0))/\(String(describing: delegate?.total ?? 0))"{
                AssetView.isHidden = true
            resetTrackingConfiguration(detectImage: (delegate?.newReferenceImages)!)
            }else{
                if AssetView.isHidden == true{
                    AssetView.isHidden = false
                }
                if counter < 5{
                    counter += 1
                }else{
                    resetTrackingConfiguration(detectImage: (delegate?.newReferenceImages)!)
                    counter = 0
                }
            }
    }
    
    func GetTarget(){
        if ChectInternet() == true{
        // self.showActivityIndicatory(uiView: self.view)
        GetTargetModel = GetTargetViewModel(user_id: user_id)
        GetTargetModel.GetTargetData { (bool, data) in
            if bool == true{
                var newReferenceImages = Set<ARReferenceImage>()
                DispatchQueue.global().async {
                    for i in 0..<data.count{
                        if let dat = try? Data(contentsOf: URL(string: imageUrl + (data[i].targetPath ?? ""))!) {
                            if let image = UIImage(data: dat) {
                                let arImage = ARReferenceImage(image.cgImage!, orientation: CGImagePropertyOrientation.up, physicalWidth: CGFloat(300.0))
                                    arImage.name = data[i].targetName
                                newReferenceImages.insert(arImage)
                              //  print(data[i].type ?? "")
                        
                        //self.pathArray.append(data[i].targetName ?? "")
                        //self.typeArray.append(data[i].type ?? "")
                       // let ratio = (100 * self.typeArray.count)/data.count
                        DispatchQueue.main.async {
                         //   self.LoadingLbl.text = "\(self.pathArray.count)/\(data.count)"
                        }
                        //print("Percentage is \(ratio)%")
                            }
                        }
                    }
                    
                //self.HideActivityIndicator(uiView: self.view)
                print(newReferenceImages.count)
                    DispatchQueue.main.async {
                    self.AssetView.isHidden = false
                    self.resetTrackingConfiguration(detectImage: newReferenceImages)
                    }
                }
            }else{
               // self.HideActivityIndicator(uiView: self.view)
            }
        }
     }
    }
   
    func resetTrackingConfiguration(detectImage:Set<ARReferenceImage>) {
       // guard let referenceImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil) else { return }
        
        let configuration = ARWorldTrackingConfiguration()
            configuration.detectionImages = detectImage
            let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
            self.sceneView.session.run(configuration, options: options)
    }
    
    
    func ConvertImagetoARReference(Img:UIImage,name:String, type: String,completion:@escaping(Set<ARReferenceImage>)->()) {
         var newReferenceImages:Set<ARReferenceImage> = Set<ARReferenceImage>();
       // if type == "dae"{
            let arImage = ARReferenceImage(Img.cgImage!, orientation: CGImagePropertyOrientation.up, physicalWidth: CGFloat(300.0))
            arImage.name = name
            newReferenceImages.insert(arImage)
        print(newReferenceImages)
      //  }
        completion(newReferenceImages)
    }
    
    func loadImageFrom(url: URL, completionHandler:@escaping(UIImage)->()) {
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        print("LOADED ASSET");
                        completionHandler(image);
                    }
                }
            }
        }
    }
    
    func DownloadTask(urlStrin: String, name : ARReferenceImage){
        let urlString = "https://ar.shoclef.com/admin_Panel/paid_models/trash.abc"
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent("trash.abc")
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        Alamofire.download(urlString, to: destination).response { response in
            if response.error == nil, let filePath = response.destinationURL?.path {
                print(filePath)
                let url = "file://" + filePath
                guard self.sceneView.session.currentFrame != nil else {
                    return
                }
               // self.Node.removeFromParentNode()
                let asset = MDLAsset(url: URL(string: url)!)
                let object = asset.object(at: 0)
                print(object.path)
               // let scene = SCNScene()
                let node = SCNNode.init(mdlObject: object)
                node.simdPosition = float3(0, 0, 0.5)
              //  let node = scene?.rootNode.childNode(withName: "Astronaut.gltf", recursively: false)
           // let plane = SCNPlane(width: name.physicalSize.width,
           //                 height: name.physicalSize.height)
              //  node = SCNNode(geometry: plane)
                self.Node = node
                self.Node.eulerAngles = SCNVector3(0.25,0.25,0.25)
                self.Node.opacity = 0
               // self.Node.position.y = 0.2
                self.Node.runAction(self.fadeAndSpinAction)
            self.sceneView.scene.rootNode.addChildNode(self.Node)
            }
        }
    }
    
}

extension ScanController: ARSCNViewDelegate {
   
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        DispatchQueue.main.async {
            guard let imageAnchor = anchor as? ARImageAnchor,
                let imageName = imageAnchor.referenceImage.name else { return }
         
//            if self.typeArray[index!] == "jpg"{
//                let image = UIImageView()
//               let myImage = image.kf.setImage(with: URL(string: "http://ar.shoclef.com/Admin_Panel/ModVidImg/" + imageName))
//               // let theImage = UIImage(data: myImage!)
//                let plane = SCNPlane(width: 1.0, height: 0.75)
//                plane.materials = [SCNMaterial()]
//                plane.materials[0].diffuse.contents = UIImage(named: imageName)
//                plane.firstMaterial?.isDoubleSided = true
//            }
            
            let width = imageAnchor.referenceImage.physicalSize.width
            let height = imageAnchor.referenceImage.physicalSize.height
            print(width)
            print(height)
            
            let index = self.delegate?.pathArray.index(of: imageName)
            let url = NSURL(string: "http://ar.shoclef.com/Admin_Panel/ModVidImg/" + imageName)
            if self.delegate?.typeArray[index!] == "mp4"{
                let mp4URL = url?.deletingPathExtension?.appendingPathExtension("mp4")
                guard let currentFrame = self.sceneView.session.currentFrame else {
                    return
                }

                let videoPlayer = AVPlayer(url: mp4URL!)

                let videoNode = SKVideoNode(avPlayer: videoPlayer)
                videoNode.play()

                let skScene = SKScene(size: CGSize(width: width, height: height))
                skScene.addChild(videoNode)

                videoNode.position = CGPoint(x: skScene.size.width/2, y: skScene.size.height/2)
                videoNode.size = skScene.size

                let tvPlane = SCNPlane(width: 1.0, height: 0.75)
                tvPlane.firstMaterial?.diffuse.contents = skScene
                tvPlane.firstMaterial?.isDoubleSided = true

                var tvPlaneNode = SCNNode(geometry: tvPlane)
                   tvPlaneNode.opacity = 0
                  tvPlaneNode.position.y = 0.2
                // tvPlaneNode.runAction(self.fadeAndSpinAction)
                var translation = matrix_identity_float4x4
                translation.columns.3.z = -1.0
                tvPlaneNode.simdTransform = matrix_multiply(currentFrame.camera.transform, translation)
                tvPlaneNode.eulerAngles = SCNVector3(Double.pi,0.1,0.1)
              //  tvPlaneNode = self.getNode(withImageName: imageName)
                
                let overlayNode = self.getNode(withImageName: imageName)
                overlayNode.opacity = 0
                overlayNode.position.y = 0.2
                overlayNode.runAction(self.fadeAndSpinAction)
                node.addChildNode(overlayNode)
            self.sceneView.scene.rootNode.addChildNode(node)
                //node.addChildNode(tvPlaneNode)
            }
            else{
                let mp4URL = url?.deletingPathExtension?.appendingPathExtension("abc")
                self.DownloadTask(urlStrin: mp4URL?.absoluteString ?? "", name: imageAnchor.referenceImage)
            }
            
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let pointOfView = sceneView.pointOfView else { return }
            if  sceneView.isNode(node, insideFrustumOf: pointOfView){
                print("Node Is Visible")
              //  guard let imageAnchor = anchor as? ARImageAnchor,
              //      let imageName = imageAnchor.referenceImage.name else { return }

            }else{
               // resetTrackingConfiguration()
                print("Node Is Not Visible")
            }
    }

//    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
//    }
    
    func getPlaneNode(withReferenceImage image: ARReferenceImage) -> SCNNode {
        let plane = SCNPlane(width: image.physicalSize.width,
                             height: image.physicalSize.height)
        let node = SCNNode(geometry: plane)
        return node
    }
    
    func getNode(withImageName name: String) -> SCNNode {
        print(name)
        return treeNode
    }
}
