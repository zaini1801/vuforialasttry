//
//  HomePageViewController.swift
//  Interactive Wall
//
//  Created by apple on 23/10/2018.
//  Copyright © 2018 Nomi Malik. All rights reserved.
//

import UIKit

class HomePageViewController: BaseViewControllerSwift, UICollectionViewDelegate, UICollectionViewDataSource {
    

    
    var lblHeadings = ["Browse", "Create  Wall", "Scan", "Interactive Advertising"]
    var ImgesString = ["BrowseHome", "WallHome", "3dMarketHome", "INtractiveAdvrHome" ]
    var UpperColorCodes = ["#fee140", "#f093fb", "#9b23ea", "#04befe"]
    var BottomColorCodes = ["#fa709a", "#f5576c", "#5f72bd", "#4481eb"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return lblHeadings.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomePageCollectionViewCell", for: indexPath) as! HomePageCollectionViewCell
        
        cell.LblCellName.text = lblHeadings[indexPath.row]
        cell.ImageShowMiddle.image = UIImage(named: ImgesString[indexPath.row])
        
        let gradient = CAGradientLayer()
        
        gradient.frame = cell.BackGroundView.bounds
        gradient.colors = [hexxStringToSingleUIColor(hex: UpperColorCodes[indexPath.row]).cgColor, hexxStringToSingleUIColor(hex: BottomColorCodes[indexPath.row]).cgColor]
        gradient.cornerRadius = 5
        cell.BackGroundView.layer.insertSublayer(gradient, at: 0)
        
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let story = UIStoryboard(name: "HomePage", bundle: nil)
        let storys = UIStoryboard(name: "InteractiveAdvertising", bundle: nil)
        if indexPath.row == 0{
            if let advertise = storys.instantiateViewController(withIdentifier: "InteractiveAdvertisingController") as? InteractiveAdvertisingController{
                present(advertise, animated: true, completion: nil)
            }
        }
        if indexPath.row == 1{
            let createWall = story.instantiateViewController(withIdentifier: "UploadTargetVC") as! UploadTargetVC
            present(createWall, animated: true, completion: nil)
        }
        if indexPath.row == 2 {
            
            let scan = storyboard?.instantiateViewController(withIdentifier: "ScanController") as! ScanController
          //  performSegue(withIdentifier: "cloud", sender: self)
        present(scan, animated: true, completion: nil)
//            let storyBoard : UIStoryboard = UIStoryboard(name: "3DMarket", bundle:nil)
//
//            let vc = storyBoard.instantiateViewController(withIdentifier: "_DMarketCategoryController") as! _DMarketCategoryController
//            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if indexPath.row == 3 {
            let storyBoard : UIStoryboard = UIStoryboard(name: "3DMarket", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "_DMarketCategoryController") as! _DMarketCategoryController
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func HomePageSettingButton(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "HomePage", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
       // self.navigationController?.pushViewController(vc, animated: true)
       self.present(vc, animated:true, completion:nil)
    }
    
    @IBAction func logoutButton(_ sender: Any) {
            UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
            UserDefaults.standard.synchronize()
            UserDefaults.standard.removeObject(forKey: "userid")
            UserDefaults.standard.removeObject(forKey: "name")
            UserDefaults.standard.removeObject(forKey: "pic")
            let story = UIStoryboard(name: "UserAuthentication", bundle: nil)
            let login = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            present(login, animated: true, completion: nil)
            self.view.removeFromSuperview()
    }
    
    
    func hexxStringToSingleUIColor (hex:String) -> UIColor {
        
        var first_color = UIColor()
         var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if cString.count != 6 {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        first_color = UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0))
        // self.backgroundColor = first_color
        return first_color
    }
}

