//
//  UploadTextureVC.swift
//  Interactive Wall
//
//  Created by user on 15/01/2019.
//  Copyright © 2019 Nomi Malik. All rights reserved.
//

import UIKit

class UploadTextureVC: MainController {
    
   
    @IBOutlet weak var BottomDown: NSLayoutConstraint!
    @IBOutlet weak var ShowImage: UIImageView!
    var picker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        dotsRectangleView()
        picker.delegate = self
    }
    
    @IBAction func AddTargetBtn(){
        if ShowImage.image != nil{
            let alert = UIAlertController(title: "Choose one", message: "", preferredStyle: .actionSheet)
            let action = UIAlertAction(title: "Change Image", style: .default) { (action) in
                self.BottomDown.constant = 0
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
            let action2 = UIAlertAction(title: "Preview Image", style: .default) { (action) in
                let newImageView = UIImageView(image: self.ShowImage.image)
                newImageView.frame = UIScreen.main.bounds
                newImageView.backgroundColor = .black
                newImageView.contentMode = .scaleAspectFit
                newImageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage(sender:)))
                newImageView.addGestureRecognizer(tap)
                self.view.addSubview(newImageView)
            }
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(action)
            alert.addAction(action2)
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
        }else{
            self.BottomDown.constant = 0
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    @objc func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    @IBAction func Swipe(pan: UIPanGestureRecognizer){
        let translation = pan.translation(in: self.view)
        if let view = pan.view {
            view.center.y = view.center.y + translation.y
            if view.center.y < 407 {
                view.center.y = 407
            }
            if view.center.y > 530{
                self.BottomDown.constant = -407
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
        }
        pan.setTranslation(CGPoint.zero, in: self.view)
    }
    
    @IBAction func TapDown(tap: UITapGestureRecognizer){
        self.BottomDown.constant = -407
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    @IBAction func OpenCamera(){
        picker.allowsEditing = false
        picker.sourceType = .camera
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func OpenGallery(){
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func Select3DModel(){
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension UploadTextureVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        // ShowImage.image = image
        // let imagePath = getDocumentsDirectory().appendingPathComponent(imageName)
        if let jpegData = image.jpegData(compressionQuality: 0.8) {
            ShowImage.image = UIImage.init(data: jpegData)
        }
        dismiss(animated: true)
    }
    
}
