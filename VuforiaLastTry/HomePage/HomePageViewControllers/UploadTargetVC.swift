//
//  UploadTargetVC.swift
//  Interactive Wall
//
//  Created by user on 15/01/2019.
//  Copyright © 2019 Nomi Malik. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation
import AVKit
import ImageIO

class UploadTargetVC: MainController,UIDocumentPickerDelegate {

    @IBOutlet weak var BottomDown: NSLayoutConstraint!
    @IBOutlet weak var ShowImage: UIImageView!
    @IBOutlet weak var ModelImage: UIImageView!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var bottomView: UIView!
    var pick: Int = 0
    var picker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        dotsRectangleView()
        picker.delegate = self
       
       // gallery.delegate = self
    }
    
   
    
    @IBAction func AddTargetBtn(){
        if ShowImage.image != nil{
            let alert = UIAlertController(title: "Choose one", message: "", preferredStyle: .actionSheet)
            let action = UIAlertAction(title: "Change Image", style: .default) { (action) in
                self.BottomDown.constant = 0
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                    self.bottomView.isHidden = false
                }
            }
            let action2 = UIAlertAction(title: "Preview Image", style: .default) { (action) in
                let newImageView = UIImageView(image: self.ShowImage.image)
                newImageView.frame = UIScreen.main.bounds
                newImageView.backgroundColor = .black
                newImageView.contentMode = .scaleAspectFit
                newImageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage(sender:)))
                newImageView.addGestureRecognizer(tap)
                self.view.addSubview(newImageView)
            }
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(action)
            alert.addAction(action2)
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
        }else{
            
            self.BottomDown.constant = 0
            UIView.animate(withDuration: 0.5) {
                self.MainView.layer.opacity = 0.2
                self.view.layoutIfNeeded()
                self.bottomView.isHidden = false
            }
        }
    }
    
    
    @objc func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    @IBAction func Swipe(pan: UIPanGestureRecognizer){
        let translation = pan.translation(in: self.view)
        if let view = pan.view {
            view.center.y = view.center.y + translation.y
            if view.center.y < 407 {
                view.center.y = 407
            }
            if view.center.y > 530{
                self.BottomDown.constant = -407
                self.bottomView.isHidden = true
                UIView.animate(withDuration: 0.5) {
                    self.MainView.layer.opacity = 1
                    self.view.layoutIfNeeded()
            }
                ChoosImage()
            }
        }
        pan.setTranslation(CGPoint.zero, in: self.view)
    }
    
    @IBAction func TapDown(tap: UITapGestureRecognizer){
        self.BottomDown.constant = -407
        self.bottomView.isHidden = true
        UIView.animate(withDuration: 0.5) {
            self.MainView.layer.opacity = 1
            self.view.layoutIfNeeded()
        }
        ChoosImage()
    }
    
   func  ChoosImage(){
        if ShowImage.image != nil && ModelImage.image == nil
        {
            ModelImage.image = #imageLiteral(resourceName: "noimage")
        }
        if ShowImage.image == nil && ModelImage.image != nil{
            ShowImage.image = #imageLiteral(resourceName: "noimage")
        }
    }

    @IBAction func OpenCamera(){
        picker = UIImagePickerController()
        pick = 0
        picker.delegate = self
        picker.allowsEditing = false
        picker.sourceType = .camera
        present(picker, animated: true, completion: nil)
    }
   
    @IBAction func OpenGallery(){
        picker = UIImagePickerController()
        pick = 1
        picker.delegate = self
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func Select3DModel(){
        picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = false
        let alert = UIAlertController(title: "Choose One", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) in
            self.pick = 2
            self.picker.sourceType = .camera
            self.present(self.picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (_) in
            self.pick = 3
            self.picker.sourceType = .photoLibrary
            self.present(self.picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Videos", style: .default, handler: { (_) in
            self.pick = 4
            self.picker.sourceType = .photoLibrary
            self.picker.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
            self.present(self.picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "3D Model", style: .default, handler: { (_) in
            let importMenu = UIDocumentPickerViewController(documentTypes: ["public.item",kUTType3dObject], in: .import)
            importMenu.delegate = self
            //importMenu.modalPresentationStyle = .formSheet
            self.present(importMenu, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
//        gallery = GalleryController()
//        gallery.delegate = self
//        present(gallery, animated: true, completion: nil)
    }
    
   
    
    var videoData = Data()
    var imageData = Data()
    var modelData = Data()
    var targetdata = Data()
    var tag = String()
    var d3 = ""
    @IBAction func SubmitBtn(){
       
        if ChectInternet() == true{
            let image = UIImage()
            if (ModelImage.image != nil) && (ShowImage.image != nil ){
                if ModelImage.image != #imageLiteral(resourceName: "noimage") || ShowImage.image != #imageLiteral(resourceName: "noimage"){
                showActivityIndicatory(uiView: self.view)
                if tag == "video"{
                    image.UploadTextureData(texture: targetdata, model: videoData, userid: user_id,tag: "video") { (data) in
                        self.UploadTargetModel = UploadTargetViewModel(data: data)
                        self.UploadTargetModel.UploadData { (bool) in
                            if bool == true{
                                self.AlertDismiss(title: "Success", message: "Target uploaded")
                            }else{
                                self.customAlert("Failed", "Target not Uploaded due to enough size")
                            }
                            self.HideActivityIndicator(uiView: self.view)
                        }
                    }
                }else if tag == "model"{
                    image.UploadTextureData(texture: targetdata, model: modelData, userid: user_id, tag: d3) { (data) in
                        self.UploadTargetModel = UploadTargetViewModel(data: data)
                        self.UploadTargetModel.UploadData { (bool) in
                            if bool == true{
                                self.AlertDismiss(title: "Success", message: "Target uploaded")
                            }else{
                                
                                self.customAlert("Failed", "Target not Uploaded due to enough size")
                            }
                            self.HideActivityIndicator(uiView: self.view)
                        }
                    }
                }else{
                    
                    image.UploadTextureData(texture: targetdata, model: imageData, userid: user_id, tag: "image") { (data) in
                        self.UploadTargetModel = UploadTargetViewModel(data: data)
                        self.UploadTargetModel.UploadData { (bool) in
                            if bool == true{
                                self.AlertDismiss(title: "Success", message: "Target uploaded")
                            }else{
                                self.customAlert("Failed", "Target not Uploaded due to enough size")
                            }
                            self.HideActivityIndicator(uiView: self.view)
                        }
                    }
                }
            }
            }else{
                customAlert("Warning", "Please choose both image")
            }
        }
    }
    
    func createThumbnailOfVideoFromRemoteUrl(url: String) -> UIImage? {
        let asset = AVAsset(url: URL(string: url)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        //Can set this to improve performance if target size is known before hand
        //assetImgGenerate.maximumSize = CGSize(width,height)
        let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }

}

extension UploadTargetVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
      
        if pick == 0{
            guard let image = info[.originalImage] as? UIImage else { return }
            let myImage = image.resizedTo1MB()
            if let jpegData = myImage?.jpegData(compressionQuality: 1) {
                targetdata = jpegData
                ShowImage.image = UIImage.init(data: jpegData)
            }
        }else if pick == 1{
            guard let image = info[.originalImage] as? UIImage else { return }
            let myImage = image.resizedTo1MB()
            if let jpegData = myImage?.jpegData(compressionQuality: 1) {
                targetdata = jpegData
                ShowImage.image = UIImage.init(data: jpegData)
            }
        }else if pick == 2{
            tag = "image"
            guard let image = info[.originalImage] as? UIImage else { return }
            let myImage = image.resizedTo1MB()
            if let jpegData = myImage?.jpegData(compressionQuality: 0.9) {
                imageData = jpegData
                ModelImage.image = UIImage.init(data: jpegData)
            }
        }
        else if pick == 3{
            tag = "image"
            guard let image = info[.originalImage] as? UIImage else { return }
            let myImage = image.resizedTo1MB()
            if let jpegData = myImage?.jpegData(compressionQuality: 0.9) {
                imageData = jpegData
                ModelImage.image = UIImage.init(data: jpegData)
            }
        }else{
                tag = "video"
                if let imageURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
                    print(imageURL.pathExtension)
                   // let mp4URL = imageURL.deletingPathExtension().appendingPathExtension("mp4")
                    if let videoData = try? Data(contentsOf: imageURL, options: .dataReadingMapped) {
                        self.videoData = videoData
                        print(videoData.description)
                    }
                    //print(mp4URL.absoluteString)
                    ModelImage.image = createThumbnailOfVideoFromRemoteUrl(url:imageURL.absoluteString)
                }
                else {
                    // Fallback on earlier versions
                }
        }
        dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        print(myURL.pathExtension)
        print("import result : \(myURL)")
        if myURL.pathExtension == "obj" || myURL.pathExtension == "dae"{
            if let modelData = try? Data(contentsOf: myURL, options: .dataReadingMapped) {
                tag = "model"
                d3 = myURL.pathExtension
                self.modelData = modelData
                ModelImage.image = #imageLiteral(resourceName: "3d")
            }else{
                customAlert("Warning", "Can Choose only .obj & .dae format 3dModels")
            }
        }
    }
    
    
    public func documentMenu(_ documentMenu:UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
  
}
extension URL{
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
   
}
