//
//  DescriptionWallVC.swift
//  Interactive Wall
//
//  Created by user on 16/01/2019.
//  Copyright © 2019 Nomi Malik. All rights reserved.
//

import UIKit

class DescriptionWallVC: MainController {

    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var DesText: UITextView!
    @IBOutlet weak var titleView: CcardView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleText.delegate = self
        DesText.delegate = self
    }
    
    @IBAction func Next(){
        
    }
}

extension DescriptionWallVC: UITextViewDelegate,UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
        titleView.SshadowOpacity = 0.3
        titleText.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
      //  titleText.attributedPlaceholder = NSAttributedString(string: "john",attributes: [NSAttributedString.Key.foregroundColor: UIColor.yellow])
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        titleText.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.becomeFirstResponder()
        if textView.text == "Hi, I'm fine..."{
            textView.text = nil
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.resignFirstResponder()
        if textView.text == nil{
            textView.text = "Hi, I'm fine..."
        }
    }
}
