//
//  SettingsPageTableViewCell.swift
//  Interactive Wall
//
//  Created by apple on 23/10/2018.
//  Copyright © 2018 Nomi Malik. All rights reserved.
//

import UIKit

class SettingsPageTableViewCell: UITableViewCell {
    
    @IBOutlet var BackGroundView: UIView!
    
    @IBOutlet var titleLbl: UILabel!
    
    @IBOutlet var leftSideShowView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
