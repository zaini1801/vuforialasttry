//
//  SettingsViewController.swift
//  Interactive Wall
//
//  Created by apple on 23/10/2018.
//  Copyright © 2018 Nomi Malik. All rights reserved.
//

import UIKit

class SettingsViewController: MainController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var backButton: UIButton!
    

    var titleString = ["My Payments", "Update Profile", "Map", "Tutorials", "Policy", "Update Password"]
    
    var TopColorCode = ["#04BEFE", "#23EFCB", "#FEE140", "#9B23EA", "#F093FB", "#F76B1C"]
    var BottomColorCode = ["#4481EB", "#43E97B", "#FA709A", "#5F72BD", "#F5576C", "#8261FA"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let origImage = UIImage(named: "BackButtonNew")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        backButton.setImage(tintedImage, for: .normal)
        backButton.tintColor = UIColor.black
        // Do any additional setup after loading the view.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleString.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsPageTableViewCell", for: indexPath) as! SettingsPageTableViewCell
        
        cell.titleLbl.text = titleString[indexPath.row]
        
        if indexPath.row == 3 || indexPath.row == 5 {
            cell.leftSideShowView.isHidden = true
        }
        
        let gradient = CAGradientLayer()
        
        gradient.frame = cell.BackGroundView.bounds
        gradient.colors = [hexxStringToSingleUIColor(hex: TopColorCode[indexPath.row]).cgColor, hexxStringToSingleUIColor(hex: BottomColorCode[indexPath.row]).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.cornerRadius = 5
        cell.BackGroundView.layer.insertSublayer(gradient, at: 0)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }

    @IBAction func backButtonPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
//        dismiss(animated: true, completion: nil)
    }
    
    func hexxStringToSingleUIColor (hex:String) -> UIColor {
        
        var first_color = UIColor()
        
        
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if cString.count != 6 {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        first_color = UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0))
        // self.backgroundColor = first_color
        return first_color
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            customAlert("Sorry", "No payment found")
        }else if indexPath.row == 1{
            if let vc = UIStoryboard(name: "UserAuthentication", bundle: nil).instantiateViewController(withIdentifier: "UpdateController") as? UpdateController{
                present(vc, animated: true, completion: nil)
            }
        }else if indexPath.row == 2{
            if let vc = storyboard?.instantiateViewController(withIdentifier: "MapController") as? MapController{
                present(vc, animated: true, completion: nil)
            }
        }else if indexPath.row == 3{
            if let vc = storyboard?.instantiateViewController(withIdentifier: "TutorialVC") as? TutorialVC{
                present(vc, animated: true, completion: nil)
            }
        }else if indexPath.row == 4{
            if let vc = storyboard?.instantiateViewController(withIdentifier: "PrivacyController") as? PrivacyController{
                present(vc, animated: true, completion: nil)
            }
        }else{
            if let vc = UIStoryboard(name: "UserAuthentication", bundle: nil).instantiateViewController(withIdentifier: "ChangePassVC") as? ChangePassVC{
                present(vc, animated: true, completion: nil)
            }
        }
    }

}
