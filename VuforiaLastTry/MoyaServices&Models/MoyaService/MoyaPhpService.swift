//
//  MoyaPhpService.swift
//  Interactive Wall
//
//  Created by user on 16/01/2019.
//  Copyright © 2019 Nomi Malik. All rights reserved.

import Foundation
import Moya
import UIKit

enum PHPServiceData {
    case uploadTexture([Moya.MultipartFormData])
    case scanTexture(Int)
    case GetAllTexture(Int)
    case deleteTarget(Int)
}


private func JSONResponseDataFormatter(_ data: Data) -> Data{
    do{
        let dataAsJson = try JSONSerialization.jsonObject(with: data)
        let prettyData = try JSONSerialization.data(withJSONObject: dataAsJson, options: .prettyPrinted)
        return prettyData
    }
    catch
    {
        return data
    }
}

let PHPDataProvide = MoyaProvider<PHPServiceData>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])


extension PHPServiceData:TargetType{
    public var baseURL: URL {
        return serverPhpURL
    }
    
    public var path: String {
        switch self {
        case .uploadTexture:
            return "UploadtoVuforia.php"
        case .scanTexture:
            return "GetTragetPath.php"
        case .GetAllTexture:
            return "GetMyTargetPath.php"
        case .deleteTarget:
            return "DeleteTragetPath.php"
        }
    }
    
    public var method: Moya.Method {
        switch self {
       
        case .uploadTexture:
            return .post
        case .scanTexture:
            return .get
        case .GetAllTexture:
            return .get
        case .deleteTarget:
            return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .uploadTexture(let data):
            return .uploadMultipart(data)
        case .scanTexture(let user_id):
            return .requestParameters(parameters: ["User_id":user_id], encoding: URLEncoding.queryString)
        case .GetAllTexture(let user_id):
            return .requestParameters(parameters: ["User_id":user_id], encoding: URLEncoding.queryString)
        case .deleteTarget(let target_id):
            return .requestParameters(parameters: ["Traget_Id":target_id], encoding: URLEncoding.queryString)
        }
    }
    
    public var headers: [String : String]? {
        return [:]
    }
}
