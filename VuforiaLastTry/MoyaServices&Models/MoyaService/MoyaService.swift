//
//  APIService.swift
//  TabBarNavigationBarSideMenu
//
//  Created by user on 10/11/2018.
//  Copyright © 2018 shoclef. All rights reserved.
//

import Foundation
import Moya
import UIKit

let serverURL = URL(string: "http://testshoclefapi.shoclef.com/Api/UserSignup.svc/")!
let serverPhpURL = URL(string: "https://ar.shoclef.com/Admin_Panel/")!
let imageUrl = "https://ar.shoclef.com/Admin_Panel/"
let imageURL = "https://ar.shoclef.com/Admin_Panel/ModVidImg/"
enum CMSServiceData {
    case login(String,String)
    case SignUP(String,String,String,String,String,Int,String,String,String,String,String,String,String,String,String,String)
    case ForgetPass(String)
    case ChangePass(Int,String,String)
    case Tutorials()
    case howtoUse()
    case Policy()
    case UpdateProfile(String,String,String,String,String,String,Int,Int)
    case ShowProfile(Int,Int)
    case MapOnFriend(user_id: Int, tag: String, lat: String, lng: String)
}


private func JSONResponseDataFormatter(_ data: Data) -> Data{
    do{
        let dataAsJson = try JSONSerialization.jsonObject(with: data)
        let prettyData = try JSONSerialization.data(withJSONObject: dataAsJson, options: .prettyPrinted)
        return prettyData
    }
    catch
    {
        return data
        
    }
}

let CMSDataProvide = MoyaProvider<CMSServiceData>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])


extension CMSServiceData:TargetType{
    public var baseURL: URL {
        return serverURL
    }
    public var path: String {
        switch self {
        case .login(_,_):
            return "Login"
        case .SignUP:
            return "signup"
        case .ForgetPass:
            return "ForgetPassword"
        case .ChangePass:
            return "ChangePassword"
        case .Tutorials:
            return "VedioTutorialData"
        case .howtoUse:
            return "Tutorials"
        case .Policy:
            return "PolicyData"
        case .UpdateProfile:
            return "UpdateProfile"
        case .ShowProfile:
            return "ShowProfile"
        case .MapOnFriend:
            return "Around_Me"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .login:
            return .post
        case .SignUP:
            return .post
        case .ForgetPass:
            return .post
        case .ChangePass:
            return .post
        case .Tutorials:
            return .post
        case .howtoUse:
            return .post
        case .Policy:
            return .post
        case . UpdateProfile:
            return .post
        case .ShowProfile:
            return .post
        case .MapOnFriend:
            return .post
        }
    }
    
    public var sampleData: Data {
        
        return Data()
    }
    public var task: Task {
        switch self {
        case .login(let username, let password):
           return .requestParameters(parameters: ["username":username,"password":password], encoding: JSONEncoding.default)
        case .SignUP(let fname, let lname,let username, let password,let email,let age, let country,let state, let city,let zip, let lat,let lng,_, let gender,let ProfilePic,let coverImage):
            return .requestParameters(parameters: ["fname" : fname, "LName": lname,"UserName":username,"Password":password,"Email": email, "Age":age, "Country": country, "State":state,"City":city,"Zip":zip,"Lat":lat,"Lng":lng,"Device_Id":"","Gender":gender,"profileimage":ProfilePic,"coverimage":coverImage], encoding: JSONEncoding.default)
        case .ForgetPass(let email):
            return .requestParameters(parameters: ["email":email], encoding: JSONEncoding.default)
        case .ChangePass(let user_id,let oldpass, let newpass):
             return .requestParameters(parameters: ["user_id":user_id,"old_password":oldpass,"new_password":newpass], encoding: JSONEncoding.default)
        case .Tutorials():
            return .requestPlain
        case .howtoUse():
            return .requestPlain
        case .Policy():
            return .requestPlain
        case .UpdateProfile(let fname, let lname,let coverImage, let profileImage,let about, let phone,let age, let user_id):
            return .requestParameters(parameters: [ "fname" : fname, "lname": lname,"coverimage":coverImage,"profileimage":profileImage,"about": about, "phoneno":phone,"age":age,"user_id":user_id], encoding: JSONEncoding.default)
        case .ShowProfile(let user_id,let profile_id):
            return .requestParameters(parameters: ["user_id":user_id,"profile_Id":profile_id], encoding: JSONEncoding.default)
        case .MapOnFriend(let user_id , let tag , let lat , let lng):
            return .requestParameters(parameters: ["user_id":user_id,"tag":tag,"lat":lat,"lng":lng], encoding: JSONEncoding.default)
        }
    }
    
    public var headers: [String : String]? {
        return ["Content-Type":"application/json; charset=utf-8"]
    }
}
