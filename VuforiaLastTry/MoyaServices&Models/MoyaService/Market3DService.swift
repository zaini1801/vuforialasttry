//
//  Market3D.swift
//  VuforiaLastTry
//
//  Created by user on 22/01/2019.
//  Copyright © 2019 Beliverz. All rights reserved.
//

import Foundation
import Moya

enum MarketServiceData {
   // case uploadTexture([Moya.MultipartFormData])
    case GetModels()
  //  case GetAllTexture(Int)
   // case deleteTarget(Int)
}


private func JSONResponseDataFormatter(_ data: Data) -> Data{
    do{
        let dataAsJson = try JSONSerialization.jsonObject(with: data)
        let prettyData = try JSONSerialization.data(withJSONObject: dataAsJson, options: .prettyPrinted)
        return prettyData
    }
    catch
    {
        return data
    }
}

let MarketDataProvide = MoyaProvider<MarketServiceData>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])


extension MarketServiceData:TargetType{
    public var baseURL: URL {
        return serverPhpURL
    }
    
    public var path: String {
        switch self {
        case .GetModels:
            return "GetModelsForDownload.php"
     
        }
    }
    
    public var method: Moya.Method {
        switch self {
            
        case .GetModels:
            return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .GetModels():
            return .requestPlain
        }
    }
    
    public var headers: [String : String]? {
        return [:]
    }
}
