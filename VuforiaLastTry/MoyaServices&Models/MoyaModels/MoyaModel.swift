//
//  MoyaModel.swift
//  Interactive Wall
//
//  Created by user on 16/01/2019.
//  Copyright © 2019 Nomi Malik. All rights reserved.
//
import Foundation
// Login

struct LoginResult: Codable {
    let loginResult: LoginResultClass?
    
    enum CodingKeys: String, CodingKey {
        case loginResult = "LoginResult"
    }
}

struct LoginResultClass: Codable {
    let message: String?
    let data: [LoginData]?
    let status: String?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case data, status
    }
}

struct LoginData: Codable {
    let coverImage, deviceToken, email, firstName: String?
    let lastName, phoneNo, profilePicture, userName: String?
    let city: String?
    let cityID: Int?
    let country: String?
    let rating: Int?
    let state: String?
    let userID: Int?
    
    enum CodingKeys: String, CodingKey {
        case coverImage = "Cover_Image"
        case deviceToken = "Device_Token"
        case email = "Email"
        case firstName = "First_Name"
        case lastName = "Last_Name"
        case phoneNo = "Phone_No"
        case profilePicture = "Profile_Picture"
        case userName = "User_Name"
        case city
        case cityID = "city_id"
        case country, rating, state
        case userID = "user_id"
    }
}


// SignUP
struct SignupResult: Codable {
    let signupResult: SignupResultClass?
}

struct SignupResultClass: Codable {
    let message, status: String?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case status
    }
}

//ForgetPass
struct ForgetPasswordResult: Codable {
    let forgetPasswordResult: ForgetPasswordResultClass?
    
    enum CodingKeys: String, CodingKey {
        case forgetPasswordResult = "ForgetPasswordResult"
    }
}

struct ForgetPasswordResultClass: Codable {
    let message, status: String?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case status
    }
}


//Change Password

struct PasswordData: Codable {
    let changePasswordResult: ChangePasswordResult
    
    enum CodingKeys: String, CodingKey {
        case changePasswordResult = "ChangePasswordResult"
    }
}

struct ChangePasswordResult: Codable {
    let message, status: String?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case status = "status"
    }
}

// GetTarget

typealias GetTargetResult = [GetTargetResultElement]

struct GetTargetResultElement: Codable {
    let targetName, targetPath: String?
    let type: String?
    
    enum CodingKeys: String, CodingKey {
        case targetName = "TargetName"
        case targetPath = "TargetPath"
        case type = "Type"
    }
}


struct UploadTargerResult: Codable {
    let status: String?
}


// Get All Targets
typealias GetAllTargetResult = [GetAllTargetResultElement]

struct GetAllTargetResultElement: Codable {
    let targetID: Int?
    let targetName, targetPath, modelName, modelPath: String?
    let createdDate: String?
    
    enum CodingKeys: String, CodingKey {
        case targetID = "TargetID"
        case targetName = "TargetName"
        case targetPath = "TargetPath"
        case modelName = "ModelName"
        case modelPath = "ModelPath"
        case createdDate = "CreatedDate"
    }
}

// Get 3D Models
typealias GetModelResults = [GetModelResult]

struct GetModelResult: Codable {
    let modelName, modelPath, modelPicture, modelPrice: String?
    
    enum CodingKeys: String, CodingKey {
        case modelName = "Model_Name"
        case modelPath = "Model_Path"
        case modelPicture = "Model_Picture"
        case modelPrice = "Model_Price"
    }
}


//Tutorials
struct TutorialResult: Codable {
    let vedioTutorialDataResult: VedioTutorialDataResult?
    
    enum CodingKeys: String, CodingKey {
        case vedioTutorialDataResult = "VedioTutorialDataResult"
    }
}

struct VedioTutorialDataResult: Codable {
    let data: [VideoTutorial]?
    let status: String?
}

struct VideoTutorial: Codable {
    let videoName: String?
    let videoMp4: String?
    let videoScreenshot: String?
    
    enum CodingKeys: String, CodingKey {
        case videoName = "Video_Name"
        case videoMp4 = "Video_mp4"
        case videoScreenshot = "Video_screenshot"
    }
}

//howtoUse

struct HowToUseResult: Codable {
    let tutorialsResult: TutorialsResult?
    
    enum CodingKeys: String, CodingKey {
        case tutorialsResult = "TutorialsResult"
    }
}

struct TutorialsResult: Codable {
    let data: [Datum]?
    let status: String?
}

struct Datum: Codable {
    let description, title: String?
}

// Privacy policy
struct PolicyDataResult: Codable {
let policyDataResult: PolicyDataResultClass?

enum CodingKeys: String, CodingKey {
    case policyDataResult = "PolicyDataResult"
}
}

struct PolicyDataResultClass: Codable {
    let data: [PrivacyDatum]?
    let status: String?
}

struct PrivacyDatum: Codable {
    let description, title: String?
    
    enum CodingKeys: String, CodingKey {
        case description = "Description"
        case title = "Title"
    }
}

//Update Profile


struct UpdateProfileResult: Codable {
    let updateResult:UpdateResultClass
    
    enum CodingKeys: String, CodingKey {
        case updateResult = "UpdateProfileResult"
    }
}

struct UpdateResultClass: Codable {
    let Message: String?
    let data: [UpdateData]?
    let status: String?
}

struct UpdateData: Codable {
    let gender, state, lastName, firstName: String?
    let age: Int?
    let zip, profilePicture, about, lat: String?
    let userID: Int?
    let userName, city, lng, phoneNo: String?
    let country, coverPicture, email: String?
    
    enum CodingKeys: String, CodingKey {
        case gender, state, lastName, firstName, age, zip, profilePicture, about, lat
        case userID = "userId"
        case userName, city, lng, phoneNo, country, coverPicture, email
    }
}

// Show Profile

struct ShowData: Codable {
    let showProfileResult: ShowProfileResult
    
    enum CodingKeys: String, CodingKey {
        case showProfileResult = "ShowProfileResult"
    }
}

struct ShowProfileResult: Codable {
    let data: [ShowProfileDatum]?
    let status: String?
}

struct ShowProfileDatum: Codable {
    let about: String?
    let age: Int?
    let lname: String?
    let fname: String?
    let rating: Int?
    let coverImage: String?
    let phoneNo: String?
    let profielImage: String?
    let isbbussiness: String?
    let isservice: String?
    let city: String?
    let country: String?
    let state : String?
    let isfollow  : String?
    let isfriend : String?
    let isfriendRequest : String?
    let user_follower1 : Int?
    let balance : Double?
    let user_id: Int?
    let totalFriends : Int?
    let RequestStatus:String?
    let conversationId:Int?
    
    enum CodingKeys: String, CodingKey {
        case about = "About"
        case age = "Age"
        case fname = "FName"
        case lname = "LName"
        case rating = "Rating"
        case coverImage = "cover_image"
        case phoneNo = "phone_no"
        case profielImage = "profiel_image"
        case isbbussiness = "isbbussiness"
        case isservice = "isservice"
        case state = "state"
        case country = "country"
        case city = "city"
        case isfollow = "isfollow"
        case isfriend = "isfriend"
        case isfriendRequest = "isfriendRequest"
        case user_follower1 = "user_follower1"
        case balance = "balance"
        case user_id = "user_id"
        case totalFriends = "totalFriends"
        case RequestStatus = "RequestStatus"
        case conversationId = "conversationId"
        
    }
}

// Friends on maps

struct AroundMe: Codable {
    let aroundMeResult: AroundMeResult?
    
    enum CodingKeys: String, CodingKey {
        case aroundMeResult = "Around_MeResult"
    }
}

struct AroundMeResult: Codable {
    let data: [AroundMeResultDatum]?
    let status: String
}

struct AroundMeResultDatum: Codable {
    let lat, lng : String?
    let address, name, profileImage: String?
    let userID: Int?
    
    enum CodingKeys: String, CodingKey {
        case address, name, profileImage
        case lat, lng
        case userID = "user_id"
    }
}
