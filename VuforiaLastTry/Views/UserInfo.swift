//
//  UserInfo.swift
//  Interactive Wall
//
//  Created by user on 16/01/2019.
//  Copyright © 2019 Nomi Malik. All rights reserved.
//

import Foundation
import UIKit
class Login{
    var username: String!
    var password: String!
    
    init(lm: LoginViewModel) {
        self.username = lm.username
        self.password = lm.password
    }
}

class SignUp{
    
    var fname : String!
    var lname : String!
    var username : String!
    var password : String!
    var email : String!
    var age : Int!
    var country : String!
    var state : String!
    var city : String!
    var zip : String!
    var lat : String!
    var lng : String!
    var device_id : String!
    var gender : String!
    var profilePic : String!
    var coverPic : String!
    
    init(sm : SignUPViewModel) {
        self.fname = sm.fname
        self.lname = sm.lname
        self.username = sm.username
        self.password = sm.password
        self.email = sm.email
        self.age = sm.age
        self.country = sm.country
        self.state = sm.state
        self.city = sm.city
        self.zip = sm.zip
        self.lat = sm.lat
        self.lng = sm.lng
        self.device_id = sm.device_id
        self.gender = sm.gender
        self.profilePic = sm.profilePic
        self.coverPic = sm.coverPic
    }
}

class ChangePassword{
    var user_id : Int!
    var oldPass : String!
    var newPass : String!
    
    init(cp:ChangePassViewModel) {
        self.user_id = cp.user_id
        self.oldPass = cp.oldPass
        self.newPass = cp.newPass
    }
}

class ForgetPassword{
    var email : String!
    
    init(fp: ForgetPassViewModel) {
        self.email = fp.email
    }
}

class UpdateProfile{
    var fname : String!
    var lname : String!
    var coverPic : String!
    var profilePic : String!
    var about : String!
    var phone : String!
    var age : Int!
    var userid : Int!
    
    init(up: UpdateProfileViewModel) {
        self.fname = up.fname
        self.lname = up.lname
        self.coverPic = up.coverPic
        self.profilePic = up.profilePic
        self.about = up.about
        self.phone = up.phone
        self.age = up.age
        self.userid = up.userid
    }
}

class ShowProfile{
    var user_id : Int!
    var profile_id : Int!
    
    init(sp: ShowProfileViewModel) {
        self.user_id = sp.user_id
        self.profile_id = sp.profile_id
    }
}
