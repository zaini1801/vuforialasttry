//
//  TargetInfo.swift
//  Interactive Wall
//
//  Created by user on 17/01/2019.
//  Copyright © 2019 Nomi Malik. All rights reserved.
//

import Foundation
import  UIKit
import Moya

class UploadTarget{
    var user_id : Int!
    var texture : [Moya.MultipartFormData]!
    var model : [Moya.MultipartFormData]!
    var data : [Moya.MultipartFormData]!
    init(ut: UploadTargetViewModel) {
        self.user_id = ut.user_id
        self.data = ut.data
    }
    
}


class GetTarget{
    var user_id : Int!
   // var cloud_id : String!
    
    init(gt: GetTargetViewModel) {
        self.user_id = gt.user_id
       // self.cloud_id = gt.cloud_id
    }
}

class GetAllTarget{
   var user_id: Int!
    
    init(gt: GetAllTargetViewModel) {
        self.user_id = gt.user_id
    }
}

class DeleteTarget{
    var Target_Id: Int!
    
    init(dt: DeleteTargetViewModel) {
        self.Target_Id = dt.Target_Id
    }
}
