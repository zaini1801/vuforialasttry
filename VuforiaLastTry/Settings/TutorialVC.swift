//
//  TutorialVC.swift
//  VuforiaLastTry
//
//  Created by user on 23/01/2019.
//  Copyright © 2019 Beliverz. All rights reserved.
//

import UIKit
import MediaPlayer

class TutorialVC: MainController,UITableViewDelegate,UITableViewDataSource {

    var Tutorial = [VideoTutorial]()
    var moviePlayer : MPMoviePlayerController!
    @IBOutlet weak var tableview:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        GetTutorials {
             self.tableview.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Tutorial.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TutorialViewCell
        cell.playBtn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PlayBtn(sender:))))
        cell.playBtn.tag = indexPath.row
        cell.VideoName.text = Tutorial[indexPath.row].videoName ?? "Title"
         let url = URL(string: "https://shoclefapi.shoclef.com" + (Tutorial[indexPath.row].videoScreenshot?.removeSpace() ?? ""))
        cell.VideoThumbnail.kf.setImage(with: url)
        return cell
    }
    
    @objc func PlayBtn(sender:UITapGestureRecognizer)
    {
        if let image = sender.view as? UIImageView
        {
            let url = URL(string: "https://shoclef.com" + (Tutorial[image.tag].videoMp4?.removeSpace() ?? ""))
            moviePlayer = MPMoviePlayerController(contentURL: url)
            moviePlayer?.view.frame = self.view.frame
            moviePlayer?.isFullscreen = true
            //moviePlayer?.controlStyle = .embedded
            moviePlayer?.prepareToPlay()
            moviePlayer?.play()
            self.view.addSubview((moviePlayer?.view)!)
        }
    }
    
    func GetTutorials(completion:@escaping()->()){
        if ChectInternet() == true{
            showActivityIndicatory(uiView: self.view)
            TutorialModel = TutorialViewModel()
            TutorialModel.Tutorials { (bool, data) in
                if bool == true{
                    self.Tutorial = data
                    self.HideActivityIndicator(uiView: self.view)
                    completion()
                }
                else{
                    self.HideActivityIndicator(uiView: self.view)
                    self.AlertDismiss(title: "Sorry", message: "No data Found")
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
