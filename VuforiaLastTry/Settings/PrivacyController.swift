//
//  PrivacyController.swift
//  VuforiaLastTry
//
//  Created by user on 23/01/2019.
//  Copyright © 2019 Beliverz. All rights reserved.
//

import UIKit

class PrivacyController: MainController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var privacyTB:UITableView!
    
    var privacyData = [PrivacyDatum]()
    override func viewDidLoad() {
        super.viewDidLoad()
        GetPrivacy {
            self.privacyTB.reloadData()
        }
    }
    
    func GetPrivacy(completion: @escaping()->()){
        if ChectInternet() == true{
            showActivityIndicatory(uiView: self.view)
            TutorialModel = TutorialViewModel()
            TutorialModel.PrivacyPolicy { (bool, data) in
                if bool == true{
                    self.privacyData = data
                    self.HideActivityIndicator(uiView: self.view)
                    completion()
                }else{
                    self.HideActivityIndicator(uiView: self.view)
                    self.AlertDismiss(title: "Sorry", message: "Privacy didn't load")
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return privacyData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "privacyCell", for: indexPath) as? PrivacyViewCell else{return UITableViewCell()}
        cell.titleName.text = self.privacyData[indexPath.row].title ?? "No Title"
        cell.selectionStyle = .none
        return cell
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
