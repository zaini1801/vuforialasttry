//
//  TermsController.swift
//  VuforiaLastTry
//
//  Created by user on 23/01/2019.
//  Copyright © 2019 Beliverz. All rights reserved.
//

import UIKit

class TermsController: MainController  {

    
    @IBOutlet weak var TermsView: UIWebView!
    let url = "https://onedrive.live.com/view.aspx?resid=806E44F08E8C2B6F!14787&ithint=file%2cdocx&app=Word&authkey=!ALuGLJk-d-yBapw"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TermsView.delegate = self as? UIWebViewDelegate
        TermsView.scrollView.isScrollEnabled = true
        TermsView.scalesPageToFit = true
        //        showLoaderwithText(text: "Loading...")
        if let url = URL(string: url) {
            self.showActivityIndicatory(uiView: self.view)
            let request = URLRequest(url: url)
            DispatchQueue.main.async {
                self.TermsView.loadRequest(request)
                self.TermsView.scrollView.decelerationRate = UIScrollView.DecelerationRate.fast
                self.HideActivityIndicator(uiView: self.view)
            }
        }
        else
        {
            
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.HideActivityIndicator(uiView: self.view)
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.HideActivityIndicator(uiView: self.view)
        AlertDismiss(title: "Sorry", message: "Page didn't load")
    }

}
