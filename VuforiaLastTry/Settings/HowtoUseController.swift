//
//  HowtoUseController.swift
//  VuforiaLastTry
//
//  Created by user on 23/01/2019.
//  Copyright © 2019 Beliverz. All rights reserved.
//

import UIKit

class HowtoUseController: MainController,UITableViewDelegate,UITableViewDataSource {

    var totalData = [Datum]()
    @IBOutlet weak var tableVIewHow:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        GetData {
            self.tableVIewHow.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return totalData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableVIewHow.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HowtoUseCell
        cell.title.text = totalData[indexPath.row].title ?? "Title"
        return cell
    }
    
    func GetData(completion:@escaping()->()){
        if ChectInternet() == true{
            showActivityIndicatory(uiView: self.view)
             TutorialModel = TutorialViewModel()
            TutorialModel.howToUse { (bool, data) in
                if bool == true{
                    self.totalData = data
                    self.HideActivityIndicator(uiView: self.view)
                    completion()
                }else{
                    self.HideActivityIndicator(uiView: self.view)
                    self.AlertDismiss(title: "Sorry", message: "No data found")
                }
            }
        }
    }
    
}
