//
//  Global.swift
//  Interactive Wall
//
//  Created by user on 17/01/2019.
//  Copyright © 2019 Nomi Malik. All rights reserved.
//

import Foundation
import UIKit
import  Moya
import ImageIO
import AVKit
import Alamofire

class GifImage{
    class func animatedImageWithGIFNamed(name: String!) -> UIImage? {
        let screenScale = Int(UIScreen.main.scale)
        let possibleScales = [1, 2, 3]
        let orderedScales = [screenScale] + possibleScales.filter{$0 != screenScale}
        
        let mapping = { (x: Int) in ["@" + String(x) + "x", "@" + String(x) + "X"]}
        let tmp = orderedScales.map(mapping)
        let orderedSuffixes = tmp.reduce([], +) + [""]
        
        for suffix in orderedSuffixes {
            if let url = Bundle.main.url(forResource: name + suffix, withExtension: "gif") {
                if let source = CGImageSourceCreateWithURL(url as CFURL, nil) {
                    return animatedImageWithImageSource(source: source)
                }
            }
        }
        
        return nil
    }
    
    private class func animatedImageWithImageSource (source: CGImageSource) -> UIImage?    {
        let (images, delays) = createImagesAndDelays(source: source);
        let totalDuration = delays.reduce(0, +)
        let frames = frameArray(images, delays, totalDuration)
        
        // All durations in GIF are in 1/100th of second
        let duration = TimeInterval(Double(totalDuration)/100.0)
        let animation = UIImage.animatedImage(with: frames, duration: duration)
        
        return animation
    }
    
    private class func createImagesAndDelays(source: CGImageSource) -> (Array<CGImage>, Array<Int>) {
        let count = Int(CGImageSourceGetCount(source))
        
        var images = Array<CGImage>()
        var delays = Array<Int>()
        
        for i in 0 ..< count {
            if let image = CGImageSourceCreateImageAtIndex(source, i, [:] as CFDictionary) {
                images.append(image)
                delays.append(delayForImageAtIndex(source, UInt(i)))
            }
        }
        
        return (images, delays)
    }
    
    private class func delayForImageAtIndex(_ source: CGImageSource, _ i: UInt) -> Int {
        var delay = 1
        
        let properties = CGImageSourceCopyPropertiesAtIndex(source, Int(i), [:] as CFDictionary)
        
        if (properties != nil) {
            let gifDictionaryProperty = unsafeBitCast(kCGImagePropertyGIFDictionary, to: UnsafeRawPointer.self)
            let gifProperties = CFDictionaryGetValue(properties, gifDictionaryProperty)
            
            if (gifProperties != nil) {
                let gifPropertiesCFD = unsafeBitCast(gifProperties, to: CFDictionary.self)
                
                let unclampedDelayTimeProperty = unsafeBitCast(kCGImagePropertyGIFUnclampedDelayTime, to: UnsafeRawPointer.self)
                var number = unsafeBitCast(CFDictionaryGetValue(gifPropertiesCFD, unclampedDelayTimeProperty), to: NSNumber.self);
                
                if (number.doubleValue == 0) {
                    let delayTimeProperty = unsafeBitCast(kCGImagePropertyGIFDelayTime, to: UnsafeRawPointer.self)
                    number = unsafeBitCast(CFDictionaryGetValue(gifPropertiesCFD, delayTimeProperty), to: NSNumber.self);
                }
                
                if (number.doubleValue > 0) {
                    delay = lrint(number.doubleValue * 100);
                }
            }
        }
        
        return delay;
    }
    
    private class func frameArray(_ images: Array<CGImage>, _ delays: Array<Int>, _ totalDuration: Int) -> [UIImage] {
        let delayGCD = gcd(values: delays)
        
        var frames = Array<UIImage>()
        frames.reserveCapacity(images.count)
        
        for i in 0 ..< images.count {
            let frame = UIImage(cgImage: images[i], scale: UIScreen.main.scale, orientation: .up)
            for _ in 0 ..< delays[i]/delayGCD {
                frames.append(frame)
            }
        }
        
        return frames;
    }
    private class func gcd(values: Array<Int>) -> Int {
        if values.count == 0 {
            return 1;
        }
        
        var currentGCD = values[0]
        
        for i in 0 ..< values.count {
            currentGCD = gcd(currentGCD, values[i])
        }
        
        return currentGCD;
    }
    
    private class func gcd(_ aNumber: Int, _ anotherNumber: Int) -> Int {
        var a = aNumber
        var b = anotherNumber
        while true {
            let r = a % b
            if r == 0 {
                return b
            }
            a = b
            b = r
        }
    }
}
extension URL{
    func sendVideo(texture:UIImage, model: URL,userid: Int,completion:()->()){
        let textureData =  texture.jpegData(compressionQuality: 0.8)
       // var formData: [Moya.MultipartFormData] = [Moya.MultipartFormData(provider: .data(textureData!), name: "texture", fileName: "abc.jpg", mimeType: "image/jpg")]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(model, withName: "Video")
           // multipartFormData.append(fo, withName: "Image")
        }, to:"http://yourserverpath.php")
        { (result) in
            switch result {
            case .success(let upload, _ , _):
                
                upload.uploadProgress(closure: { (progress) in
                    
                    print("uploding")
                })
                
                upload.responseJSON { response in
                    
                    print("done")
                    
                }
                
            case .failure(let encodingError):
                print("failed")
                print(encodingError)
                
            }
        }
    }
}
extension String{
    
    func removeSpace() ->String
    {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed) ?? ""
    }
    
}
extension UIImage{
    func UploadTextureData(texture: Data,model: Data,userid: Int,tag: String, completion: @escaping([Moya.MultipartFormData])->()){
       // let textureData =  texture.jpegData(compressionQuality: 0.8)
       // let modelData =  model.jpegData(compressionQuality: 0.8)
        
        var formData: [Moya.MultipartFormData] = [Moya.MultipartFormData(provider: .data(texture), name: "texture", fileName: "abc.jpg", mimeType: "image/jpg")]
        if tag == "video"{
            formData.append (Moya.MultipartFormData(provider: .data(model), name: "model", fileName: "abc.mp4", mimeType: "video/mp4"))
        }else if tag == "image"{
           formData.append (Moya.MultipartFormData(provider: .data(model), name: "model", fileName: "abc.jpg", mimeType: "image/jpg"))
        }else{
            print(tag)
            formData.append (Moya.MultipartFormData(provider: .data(model), name: "model", fileName: "abc.\(tag)", mimeType: "object/\(tag)"))
            }
         formData.append(Moya.MultipartFormData(provider: .data(String(userid).data(using: String.Encoding.utf8) ?? Data()), name: "UserID"))
        completion(formData)
    }
}

extension UIImage{
    
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func resizedTo1MB() -> UIImage? {
        guard let imageData = self.pngData() else { return nil }
        
        var resizingImage = self
        var imageSizeKB = Double(imageData.count) / 2000.0 // ! Or devide for 1024 if you need KB but not kB
        
        while imageSizeKB > 2000 { // ! Or use 1024 if you need KB but not kB
            guard let resizedImage = resizingImage.resized(withPercentage: 0.9),
                let imageData = resizedImage.pngData()
                else { return nil }
            
            resizingImage = resizedImage
            imageSizeKB = Double(imageData.count) / 2000.0 // ! Or devide for 1024 if you need KB but not kB
        }
        
        return resizingImage
    }
    
    
    
    func convertBaser64Withoutsize(image:UIImage) -> String
    {
        
        let images = resizeimage(image: image, withSize: CGSize(width: 500, height: 500))
        var strBase64 = String()
        if let data = images.jpegData(compressionQuality: 0.8)
        {
            strBase64 = data.base64EncodedString(options: .lineLength76Characters)
            
        }
        return strBase64
        
    }
    func resizeimage(image:UIImage,withSize:CGSize) -> UIImage {
        var actualHeight:CGFloat = image.size.height
        var actualWidth:CGFloat = image.size.width
        let maxHeight:CGFloat = withSize.height
        let maxWidth:CGFloat = withSize.width
        var imgRatio:CGFloat = actualWidth/actualHeight
        let maxRatio:CGFloat = maxWidth/maxHeight
        let compressionQuality = 1.0
        if (actualHeight>maxHeight||actualWidth>maxWidth) {
            if (imgRatio<maxRatio){
                //adjust width according to maxHeight
                imgRatio = maxHeight/actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }else if(imgRatio>maxRatio){
                // adjust height according to maxWidth
                imgRatio = maxWidth/actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }else{
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rec:CGRect = CGRect(x:0.0,y:0.0,width:actualWidth,height:actualHeight)
        UIGraphicsBeginImageContext(rec.size)
        image.draw(in: rec)
        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData = image.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        let resizedimage = UIImage(data: imageData!)
        return resizedimage!
    }
    
    func zoomImage(image: UIImage, uiView: UIView){
        let newImageView = UIImageView(image: image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage(sender:)))
        newImageView.addGestureRecognizer(tap)
        uiView.addSubview(newImageView)
    }
    
    @objc func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    func createThumbnailOfVideoFromRemoteUrl(url: String) -> UIImage? {
        let asset = AVAsset(url: URL(string: url)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        //Can set this to improve performance if target size is known before hand
        //assetImgGenerate.maximumSize = CGSize(width,height)
        let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}

