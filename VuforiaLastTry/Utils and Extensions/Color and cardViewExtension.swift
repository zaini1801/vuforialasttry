//
//  cardViewExtension.swift
//  Shoclef
//
//  Created by Nomi Malik on 05/05/2018.
//  Copyright © 2018 Shoclef. All rights reserved.
//

import Foundation
import UIKit

extension UIView
{
    
    
    
    
  ////// To setup Cardview and remove them
    func setViewCard() {
        self.backgroundColor = UIColor.white
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.8
        self.layer.cornerRadius = 10.0
    }
    
    func removeCardView() {
        self.layer.masksToBounds = false
        self.backgroundColor = UIColor.clear
        self.layer.shadowOpacity = 0.0
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowColor = UIColor.clear.cgColor
    }
    
    
    
    func gradientBackgroundWithHexString(position:Bool,firstColor: String, secondColor: String){
        clipsToBounds = true
        if self.layer.sublayers != nil
        {
            //print(self.layer.sublayers?.count)
            self.layer.sublayers!.remove(at: 0)
        }
        
        
        let color = hexxStringToUIColor(hex: [firstColor,secondColor])
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [color.0.cgColor, color.1.cgColor]
        gradientLayer.frame = self.bounds
        if position == true
        {
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        }
        else
        {
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        }
        //print(gradientLayer.frame)
        
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func hexxStringToUIColor (hex:[String]) -> (UIColor,UIColor) {
        
        var first_color = UIColor()
        var second_color = UIColor()
        for i in 0..<hex.count
        {
            var cString:String = hex[i].trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
            
            if (cString.hasPrefix("#")) {
                cString.remove(at: cString.startIndex)
            }
            
            if ((cString.count) != 6) {
                return (UIColor.gray,UIColor.gray)
            }
            
            var rgbValue:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)
            
            if i == 0
            {
                first_color = UIColor(
                    red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                    green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                    blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                    alpha: CGFloat(1.0))
            }
            else
            {
                second_color = UIColor(
                    red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                    green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                    blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                    alpha: CGFloat(1.0))
            }
            
        }
        return (first_color,second_color)
    }
    
}
