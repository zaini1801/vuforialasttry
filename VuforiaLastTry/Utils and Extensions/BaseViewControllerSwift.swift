//
//  BaseViewControllerSwift.swift
//  Interactive Wall
//
//  Created by Umer Khalid on 22/10/2018.
//  Copyright © 2018 Nomi Malik. All rights reserved.
//

import UIKit
import AVFoundation

class BaseViewControllerSwift: UIViewController {
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func customAlert(_ title:String, _ messages:String)
    {
        let errorAlert = UIAlertController(title: title, message: messages, preferredStyle: .alert)
        let OK = UIAlertAction(title: "OK", style: .default, handler: nil)
        errorAlert.addAction(OK)
        let titleFont = [ NSAttributedString.Key.font : UIFont(name: "Roboto-Medium", size: 18)! ]
        let messageFont = [ NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 14)! ]
        let attributedTitle = NSMutableAttributedString(string: title, attributes: titleFont)
        let attributedMessage = NSMutableAttributedString(string: messages, attributes: messageFont)
        errorAlert.setValue(attributedTitle, forKey: "attributedTitle")
        errorAlert.setValue(attributedMessage, forKey: "attributedMessage")
        present(errorAlert, animated: true)
        
    }
}



