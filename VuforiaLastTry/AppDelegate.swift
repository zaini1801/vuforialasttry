//
//  AppDelegate.swift
//  VuforiaLastTry
//
//  Created by user on 18/01/2019.
//  Copyright © 2019 Beliverz. All rights reserved.
//

import UIKit
import IQKeyboardManager
import Kingfisher
import GoogleMaps
import ARKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
     var newReferenceImages = Set<ARReferenceImage>()
    var pathArray = [String]()
    var typeArray = [String]()
    var total : Int = 0
    @objc var glResourceHandler: SampleGLResourceHandler?
    var user_id : Int = 0
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared().isEnabled = true
        GMSServices.provideAPIKey("AIzaSyDzQaMWzObCZO5XB-oDBstNtK5oD2Hsn7c")
       // GMSPlacesClient.provideAPIKey("AIzaSyDzQaMWzObCZO5XB-oDBstNtK5oD2Hsn7c")
        let userLoginStatus = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        
        if(userLoginStatus)
        {
            user_id = UserDefaults.standard.object(forKey: "userid") as? Int ?? 0
            let storyBoard:UIStoryboard = UIStoryboard(name: "HomePage", bundle: nil)
            let exampleVC = storyBoard.instantiateViewController(withIdentifier: "HomePageViewController") as! HomePageViewController
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = exampleVC
            self.window?.makeKeyAndVisible()
            self.GetTarget()
        }
        return true
    }

    func GetTarget(){
            var GetTargetModel : GetTargetViewModel!
            // self.showActivityIndicatory(uiView: self.view)
            GetTargetModel = GetTargetViewModel(user_id: 8)
            GetTargetModel.GetTargetData { (bool, data) in
                if bool == true{
                    self.pathArray.removeAll()
                    self.typeArray.removeAll()
                    self.total = data.count
                    DispatchQueue.global().async {
                        for i in 0..<data.count{
                            if let dat = try? Data(contentsOf: URL(string: imageUrl + (data[i].targetPath ?? ""))!) {
                            if let image = UIImage(data: dat) {
                                let arImage = ARReferenceImage(image.cgImage!, orientation: CGImagePropertyOrientation.up, physicalWidth: CGFloat(300.0))
                                arImage.name = data[i].targetName
                            self.newReferenceImages.insert(arImage)
                    self.pathArray.append(data[i].targetName ?? "")
                    self.typeArray.append(data[i].type ?? "")
                    print("\(self.typeArray.count)/\(data.count)")
                        DispatchQueue.main.async {
                    NotificationCenter.default.post(name: .didReceiveData, object: nil)
                                }
                                }
                            }
                        }
                      //  print(self.newReferenceImages.count)
                      //  DispatchQueue.main.async {
                    //self.resetTrackingConfiguration(detectImage: newReferenceImages)
                    
                      //  }
                    }
                }else{
                    // self.HideActivityIndicator(uiView: self.view)
                }
            }
        
    }
    
    
    @objc func GetData(cloud_id:String,completion:@escaping(URL,String)->()){
        //"bcdca00cbb2e4657b4bb9f790f960b03"
//        let getTarget = GetTargetViewModel(user_id: user_id, cloud_id: cloud_id)
//        getTarget.GetTargetData { (bool, data) in
//            if bool == true{
//                self.url = URL(string: imageUrl + data[2])!
//                completion(self.url!,data[5])
//            }else{
//                if let url = self.url{
//                    completion(url,"")
//                }
//            }
//        }
    }
    
    var url : URL? = URL(string: "")
    @objc func downloadImageAndReturn(_ imgName:String,_ complp: @escaping (UIImage?)->Void){
        
            KingfisherManager.shared.retrieveImage(with: self.url!, options: [], progressBlock: { (start, total) in
                let percentage = ((start/total) * 100)
                print(percentage)
            }) { (result) in
                switch result{
                case .failure(_):
                    complp(nil)
                case .success(let imgd):
                    complp(imgd.image)
                }
            }
        
    }
    
    @objc func downloadVideoAndReturn(_ complp: @escaping (URL?)->Void){
       
            complp(self.url)
        
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        if (glResourceHandler != nil) {
            // Delete OpenGL resources (e.g. framebuffer) of the SampleApp AR View
            glResourceHandler?.freeOpenGLESResources()
            glResourceHandler?.finishOpenGLESCommands()
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
}

extension Notification.Name {
    static let didReceiveData = Notification.Name("didReceiveData")
}
