//
//  3DMarketCategoryCell.swift
//  Interactive Wall
//
//  Created by apple on 25/10/2018.
//  Copyright © 2018 Nomi Malik. All rights reserved.
//

import UIKit

class _DMarketCategoryCell: UICollectionViewCell {
    
    @IBOutlet var ImageShowMain: UIImageView!
    
    @IBOutlet var LblPrise: UILabel!
    
    @IBOutlet var LblBottom: UILabel!
    
    @IBOutlet var bottomView: UIView!
}
