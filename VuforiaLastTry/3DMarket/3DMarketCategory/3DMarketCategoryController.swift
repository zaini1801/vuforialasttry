//
//  3DMarketCategoryController.swift
//  Interactive Wall
//
//  Created by apple on 25/10/2018.
//  Copyright © 2018 Nomi Malik. All rights reserved.
//

import UIKit
import DropDown
import Kingfisher

class _DMarketCategoryController: MainController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    //@IBOutlet var categoryLbl: UILabel!
  //  @IBOutlet var dropDownCategoryView: UIView!
    var data = [GetModelResult]()
    @IBOutlet var CollectionViewCategory: UICollectionView!
    
    var bottomViewTopColorCode = ["00F2FE", "F5576C", "FBAB7E", "FF7EB3", "764BA2", "38F9D7"]
    var bootomViewLowerColorCode = ["4FACFE", "F093FB", "F7CE68", "FF758C", "667EEA", "43E97B"]
    
    var counter = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        CollectionViewCategory.refreshControl = refresh
        refresh.addTarget(self, action: #selector(ReloadData), for: .touchUpInside)
        ReloadData()
     //   let tap = UITapGestureRecognizer(target: self, action: #selector(self.dropDownButton(_:)))
     //   dropDownCategoryView.addGestureRecognizer(tap)
      //  dropDownCategoryView.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
    }
    
    @objc func ReloadData(){
        GetAllModels {
            self.refresh.endRefreshing()
        }
    }
    
    func DropDowntouch(_ sender: UITapGestureRecognizer) {
        print("Hello")
        
        let dropDown = DropDown()
       // dropDown.anchorView = dropDownCategoryView
        dropDown.dataSource = ["A", "B", "C", "D","E","F","G"]
        dropDown.show()
        dropDown.textFont = (UIFont(name: "Roboto-Regular", size: 15.0))!
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
         //   self.categoryLbl.text = item
//            self.categoryLbl.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            
        }
    }
    
    func GetAllModels(completion:@escaping()->()){
        if ChectInternet() == true{
            self.showActivityIndicatory(uiView: self.view)
            GetModelsModel = GetModelViewModel()
            GetModelsModel.GetModelsData { (bool, data) in
                if bool == true{
                    self.data = data
                    self.CollectionViewCategory.reloadData()
                    self.HideActivityIndicator(uiView: self.view)
                    completion()
                }else{
                    self.HideActivityIndicator(uiView: self.view)
                    self.AlertDismiss(title: "Sorry", message: "No data found")
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "_DMarketCategoryCell", for: indexPath) as! _DMarketCategoryCell
        cell.LblPrise.text = "$" + (data[indexPath.row].modelPrice ?? "")
        if data[indexPath.row].modelPrice == "0"{
          cell.LblPrise.text = "Free"
        }
        cell.ImageShowMain.kf.setImage(with: URL(string: imageUrl + (data[indexPath.row].modelPicture ?? "")))
        cell.LblBottom.text = data[indexPath.row].modelName
        if counter == 5 {
            counter = 0
        }
        print("Counter Befor Incremnt : \(counter)\n Color Code is :\(bottomViewTopColorCode[counter])")
        counter += 1
        let gradient = CAGradientLayer()
        
        gradient.frame = cell.bottomView.bounds
        gradient.colors = [hexxStringToSingleUIColor(hex: bottomViewTopColorCode[counter]).cgColor, hexxStringToSingleUIColor(hex: bootomViewLowerColorCode[counter]).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.cornerRadius = 5
        cell.bottomView.layer.insertSublayer(gradient, at: 0)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let url = URL(string: imageUrl + (data[indexPath.row].modelPicture ?? ""))!
        let req = NSMutableURLRequest(url:url)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self as? URLSessionDelegate, delegateQueue: OperationQueue.main)
        
        let task : URLSessionDownloadTask = session.downloadTask(with: req as URLRequest)
        task.resume()
    }
    
    @IBAction func dropDownButton(_ sender: Any) {
        
        let dropDown = DropDown()
      //  dropDown.anchorView = dropDownCategoryView
        dropDown.dataSource = ["A", "B", "C", "D","E","F","G"]
        dropDown.show()
        dropDown.textFont = (UIFont(name: "Roboto-Regular", size: 15.0))!
      //  dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
      //      self.categoryLbl.text = item
//            self.categoryLbl.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
            
    //    }
    }
    

    @IBAction func backButton(_ sender: Any) {
        
       // self.navigationController?.popViewController(animated: true)
    }
    

    func hexxStringToSingleUIColor (hex:String) -> UIColor {
        
        var first_color = UIColor()
        
        
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if cString.count != 6 {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        first_color = UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0))
        // self.backgroundColor = first_color
        return first_color
    }
}
