//
//  MarketCategoryDetailController.swift
//  Interactive Wall
//
//  Created by apple on 25/10/2018.
//  Copyright © 2018 Nomi Malik. All rights reserved.
//

import UIKit
import MediaPlayer

class MarketCategoryDetailController: MainController {
    
    
    @IBOutlet var scroolView: UIScrollView!
    

    @IBOutlet var popUpview: CcardView!
    @IBOutlet var Lbltitle: UILabel!
    @IBOutlet var LblDetail: UILabel!
    @IBOutlet weak var BottomDown: NSLayoutConstraint!
    @IBOutlet weak var ShowImage: UIImageView!
    var picker = UIImagePickerController()
    var mediapicker = MPMediaPickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dotsRectangleView()
        popUpview.isHidden = true
        scroolView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.scrollViewTouch(sender:))))
        picker.delegate = self
        mediapicker.delegate = self as! MPMediaPickerControllerDelegate
    }
    

    
    @IBAction func uploadFromGalleyButton(_ sender: Any) {
    }
    
    @IBAction func selectVoiceButton(_ sender: Any) {
    }
    
    @IBAction func menuButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButton(_ sender: Any) {
        
        popUpview.isHidden = false
    }
    
    @objc func scrollViewTouch(sender:UITapGestureRecognizer)
    {
        self.resignFirstResponder()
        
        UIView.transition(with: popUpview, duration: 1, options: .transitionCrossDissolve, animations: {
            self.popUpview.isHidden = true
        }, completion: nil)
        
        scroolView.isScrollEnabled = true
    }
    
    @IBAction func AddTargetBtn(){
        if ShowImage.image != nil{
            let alert = UIAlertController(title: "Choose one", message: "", preferredStyle: .actionSheet)
            let action = UIAlertAction(title: "Change Image", style: .default) { (action) in
                self.BottomDown.constant = 0
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
            let action2 = UIAlertAction(title: "Preview Image", style: .default) { (action) in
                let newImageView = UIImageView(image: self.ShowImage.image)
                newImageView.frame = UIScreen.main.bounds
                newImageView.backgroundColor = .black
                newImageView.contentMode = .scaleAspectFit
                newImageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage(sender:)))
                newImageView.addGestureRecognizer(tap)
                self.view.addSubview(newImageView)
            }
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(action)
            alert.addAction(action2)
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
        }else{
            self.BottomDown.constant = 0
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    @objc func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    @IBAction func Swipe(pan: UIPanGestureRecognizer){
        let translation = pan.translation(in: self.view)
        if let view = pan.view {
            view.center.y = view.center.y + translation.y
            if view.center.y < 432 {
                view.center.y = 432
            }
            if view.center.y > 530{
                self.BottomDown.constant = -432
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
        }
        pan.setTranslation(CGPoint.zero, in: self.view)
    }
    
    @IBAction func TapDown(tap: UITapGestureRecognizer){
        self.BottomDown.constant = -432
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func OpenGallery(){
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    }
    @IBAction func OpenAudio(){
        mediapicker = MPMediaPickerController.self(mediaTypes:MPMediaType.music)
        mediapicker.allowsPickingMultipleItems = false
        self.present(mediapicker, animated: true, completion: nil)
    }
}

extension MarketCategoryDetailController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        // ShowImage.image = image
        // let imagePath = getDocumentsDirectory().appendingPathComponent(imageName)
        if let jpegData = image.jpegData(compressionQuality: 0.8) {
            ShowImage.image = UIImage.init(data: jpegData)
        }
        dismiss(animated: true)
    }
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        self.dismiss(animated: true, completion: nil)
        print("you picked: \(mediaItemCollection)")
    }

    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
